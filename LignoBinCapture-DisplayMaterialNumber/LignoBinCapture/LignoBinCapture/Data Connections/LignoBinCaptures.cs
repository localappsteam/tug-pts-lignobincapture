﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LignoBinCapture
{
    public class LignoBinCaptures : OraConnection
    {
        private string _oraConn;
        private OracleConnection conn;

        private string _sqlConn;
        private SqlConnection sqlconn;

        private string _sapBinId;

        #region Get Sap Bin ID from GUI
        public string SapBinId
        {
            get { return _sapBinId; }
            set { _sapBinId = value; }
        }
        #endregion

        private string _gradeCode;

        #region Get Grade_Code from GUI
        public string GradeCode
        {
            get { return _gradeCode; }
            set { _gradeCode = value; }
        }
        #endregion

        private string _stationCode;

        #region Get Sap Bin ID from GUI
        public string StationCode
        {
            get { return _stationCode; }
            set { _stationCode = value; }
        }
        #endregion

        private string _quality;

        #region Get Sap Bin ID from GUI
        public string Quality
        {
            get { return _quality; }
            set { _quality = value; }
        }
        #endregion

        private string _materialNo;

        #region Get Sap Bin ID from GUI
        public string MaterialNo
        {
            get { return _materialNo; }
            set { _materialNo = value; }
        }
        #endregion

        private string _labelData;

        #region Get Sap Bin ID from GUI
        public string LabelData
        {
            get { return _labelData; }
            set { _labelData = value; }
        }
        #endregion

        private string _username;
        private string _password;
        private string _empno;
        private string _scalestationcode;
        private string _reasondetails;
        public String Username
        {
            get { return _username; }
            set { _username = value; }
        }
        public String Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public string EmpNo
        {
            get { return _empno; }
            set { _empno = value; }
        }
        public string ScaleStationCode
        {
            get { return _scalestationcode; }
            set { _scalestationcode = value; }
        }
        public string ReasonDetails
        {
            get { return _reasondetails; }
            set { _reasondetails = value; }
        }

        private bool _authboxstatetank;

        public bool AuthBoxStateTank
        {
            get { return _authboxstatetank; }
            set { _authboxstatetank = value; }
        }

        private bool _authBoxstateproduct;

        public bool AuthBoxStateProduct
        {
            get { return _authBoxstateproduct; }
            set { _authBoxstateproduct = value; }
        }


        #region Constructor
        public LignoBinCaptures()
        {
            _oraConn = OraConnectionString();
            _sqlConn = MSSQLConnectionString();
        }
        #endregion

        #region Unused code
        //#region  Writes to scanned results to ligno powder recovery Table
        //public Boolean RegisterBin()
        //{
        //    try
        //    {
        //        conn = new OracleConnection(_oraConn);

        //        var cmd = new OracleCommand("pck_Bin_Capture.CommitBinCap", conn);

        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.BindByName = true;

        //        OracleParameter cp1 = new OracleParameter();
        //        cp1.ParameterName = "p_station_code";
        //        cp1.Size = 200;
        //        cp1.OracleDbType = OracleDbType.Varchar2;
        //        cp1.Direction = ParameterDirection.Input;
        //        cp1.Value = Properties.Settings.Default.ScanWorkStation.Trim().ToUpper();
        //        cmd.Parameters.Add(cp1);

        //        OracleParameter cp2 = new OracleParameter();
        //        cp2.ParameterName = "istrBinNum";
        //        cp2.Size = 200;
        //        cp2.OracleDbType = OracleDbType.Varchar2;
        //        cp2.Direction = ParameterDirection.Input;
        //        cp2.Value = this._sapBinId.Trim().ToUpper();
        //        cmd.Parameters.Add(cp2);


        //        OracleParameter cp3 = new OracleParameter();
        //        cp3.ParameterName = "p_quality";
        //        cp3.Size = 200;
        //        cp3.OracleDbType = OracleDbType.Varchar2;
        //        cp3.Direction = ParameterDirection.Input;
        //        cp3.Value = this._quality.Trim().ToUpper();
        //        cmd.Parameters.Add(cp3);

        //        OracleParameter cp4 = new OracleParameter();
        //        cp4.ParameterName = "p_grade_code";
        //        cp4.Size = 200;
        //        cp4.OracleDbType = OracleDbType.Varchar2;
        //        cp4.Direction = ParameterDirection.Input;
        //        cp4.Value = this._gradeCode.Trim().ToUpper();
        //        cmd.Parameters.Add(cp4);

        //        OracleParameter cp5 = new OracleParameter();
        //        cp5.ParameterName = "p_material_no";
        //        cp5.Size = 200;
        //        cp5.OracleDbType = OracleDbType.Varchar2;
        //        cp5.Direction = ParameterDirection.Input;
        //        cp5.Value = this._materialNo.Trim().ToUpper();
        //        cmd.Parameters.Add(cp5);


        //        /*OracleParameter cp1 = new OracleParameter();
        //        cp1.ParameterName = "result";
        //        cp1.OracleDbType = OracleDbType.Varchar2;
        //        cp1.Direction = ParameterDirection.ReturnValue;
        //        cp1.Size = 200;
        //        cmd.Parameters.Add(cp1);*/

        //        cmd.Connection.Open();
        //        cmd.ExecuteScalar();

        //        //string returnValue = cmd.Parameters["result"].Value.ToString();

        //        cmd.Connection.Close();
        //        cmd.Dispose();

        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
        //#endregion

        //# region Does Check for Bin in TBL_PTS_Transactions
        //public string GetScannedresultsCheck()
        //{
        //    conn = new OracleConnection(_oraConn);

        //    var cmd = new OracleCommand("lignosul.pck_ligno_powder_recovery.scannedBincheck", conn);

        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.BindByName = true;

        //    OracleParameter cp = new OracleParameter();
        //    cp.ParameterName = "p_SapId";
        //    cp.Size = 200;
        //    cp.OracleDbType = OracleDbType.Varchar2;
        //    cp.Direction = ParameterDirection.Input;
        //    cp.Value = this._sapBinId.Trim().ToUpper(); //test sapid Bin "WT24S003A6";

        //    cmd.Parameters.Add(cp);

        //    OracleParameter cp1 = new OracleParameter();
        //    cp1.ParameterName = "results";
        //    cp1.OracleDbType = OracleDbType.Varchar2;
        //    cp1.Direction = ParameterDirection.ReturnValue;
        //    cp1.Size = 200;

        //    cmd.Parameters.Add(cp1);

        //    cmd.Connection.Open();
        //    cmd.ExecuteScalar();

        //    string returnValue = cmd.Parameters["results"].Value.ToString();

        //    cmd.Connection.Close();
        //    cmd.Dispose();

        //    return returnValue;
        //}
        //#endregion

        //#region Does Check for Bin in TBL_TEMP_INV_BinS
        //public string GetBinNoIfExistsTempInvBins()
        //{
        //    conn = new OracleConnection(_oraConn);

        //    var cmd = new OracleCommand("pck_Bin_Capture.BinNoExistsTempInvBins", conn);

        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.BindByName = true;

        //    OracleParameter cp = new OracleParameter();
        //    cp.ParameterName = "BinNo_Exists";
        //    cp.Size = 200;
        //    cp.OracleDbType = OracleDbType.Varchar2;
        //    cp.Direction = ParameterDirection.Input;
        //    cp.Value = this._sapBinId.Trim().ToUpper(); //test sapid Bin "WT24S003A6";

        //    cmd.Parameters.Add(cp);

        //    OracleParameter cp1 = new OracleParameter();
        //    cp1.ParameterName = "result";
        //    cp1.OracleDbType = OracleDbType.Varchar2;
        //    cp1.Direction = ParameterDirection.ReturnValue;
        //    cp1.Size = 200;

        //    cmd.Parameters.Add(cp1);

        //    cmd.Connection.Open();
        //    cmd.ExecuteScalar();

        //    string returnValue = cmd.Parameters["result"].Value.ToString();

        //    cmd.Connection.Close();
        //    cmd.Dispose();

        //    return returnValue;
        //}
        //#endregion

        //#region Does Check for Bin in TBL_TEMP_INV_BinS
        //public string GetBinNoIfExistsPTSTransBins()
        //{
        //    conn = new OracleConnection(_oraConn);

        //    var cmd = new OracleCommand("pck_Bin_Capture.BinNoExistsPTSTransBins", conn);

        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.BindByName = true;

        //    OracleParameter cp = new OracleParameter();
        //    cp.ParameterName = "BinNo_Exists";
        //    cp.Size = 200;
        //    cp.OracleDbType = OracleDbType.Varchar2;
        //    cp.Direction = ParameterDirection.Input;
        //    cp.Value = this._sapBinId.Trim().ToUpper(); //test sapid Bin "WT24S003A6";

        //    cmd.Parameters.Add(cp);

        //    OracleParameter cp1 = new OracleParameter();
        //    cp1.ParameterName = "result";
        //    cp1.OracleDbType = OracleDbType.Varchar2;
        //    cp1.Direction = ParameterDirection.ReturnValue;
        //    cp1.Size = 200;

        //    cmd.Parameters.Add(cp1);

        //    cmd.Connection.Open();
        //    cmd.ExecuteScalar();

        //    string returnValue = cmd.Parameters["result"].Value.ToString();

        //    cmd.Connection.Close();
        //    cmd.Dispose();

        //    return returnValue;
        //}
        //#endregion
        //    #region Get Bin Details For TextBoxes
        //    public DataTable GetBindetailsForTxtBox()
        //{
        //    conn = new OracleConnection(_oraConn);
        //    var dt = new DataTable();
        //    var cmd = new OracleCommand("pck_Bin_Capture.GetTempInvBinCapDetails", conn);

        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.BindByName = true;

        //    OracleParameter cp = new OracleParameter();
        //    cp.ParameterName = "pBinNo";
        //    cp.Size = 200;
        //    cp.OracleDbType = OracleDbType.Varchar2;
        //    cp.Direction = ParameterDirection.Input;
        //    cp.Value = this._sapBinId.Trim().ToUpper(); //test sapid Bin "WT24S003A6";

        //    cmd.Parameters.Add(cp);

        //    OracleParameter cp1 = new OracleParameter();
        //    cp1.ParameterName = "results";
        //    cp1.OracleDbType = OracleDbType.RefCursor;
        //    cp1.Direction = ParameterDirection.ReturnValue;
        //    cp1.Size = 200;

        //    cmd.Parameters.Add(cp1);
        //    cmd.Connection.Open();
        //    cmd.ExecuteReader();

        //    using (OracleDataAdapter da = new OracleDataAdapter(cmd))
        //    {
        //        da.Fill(dt);
        //    }

        //    cmd.Connection.Close();
        //    cmd.Dispose();
        //    return dt;
        //}
        //    #endregion



        //#region Get Product Details For TextBoxes
        //public DataTable GetProductdDetailsForTxtBox()
        //{
        //    conn = new OracleConnection(_oraConn);
        //    var dt = new DataTable();
        //    var cmd = new OracleCommand("pck_Bin_Capture.GetSelectedBinProductDetails", conn);

        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.BindByName = true;

        //    OracleParameter cp = new OracleParameter();
        //    cp.ParameterName = "pGradeCode";
        //    cp.Size = 200;
        //    cp.OracleDbType = OracleDbType.Varchar2;
        //    cp.Direction = ParameterDirection.Input;
        //    cp.Value = this._gradeCode.Trim().ToUpper(); //test sapid Bin "WT24S003A6";

        //    cmd.Parameters.Add(cp);

        //    OracleParameter cp1 = new OracleParameter();
        //    cp1.ParameterName = "results";
        //    cp1.OracleDbType = OracleDbType.RefCursor;
        //    cp1.Direction = ParameterDirection.ReturnValue;
        //    cp1.Size = 200;

        //    cmd.Parameters.Add(cp1);
        //    cmd.Connection.Open();
        //    cmd.ExecuteReader();

        //    using (OracleDataAdapter da = new OracleDataAdapter(cmd))
        //    {
        //        da.Fill(dt);
        //    }

        //    cmd.Connection.Close();
        //    cmd.Dispose();
        //    return dt;
        //}
        //#endregion
        //Get EST Load mass check
        //Get EST Unloaded MASS check
        //  :result := pck_flowbin_scale.checkloadedweight(p_weight => :p_weight,p_station_code => :p_station_code);
        //public Boolean CheckLoadedWeight(int loadedweight)
        //{
        //    Boolean check = false;
        //    conn = new OracleConnection(_oraConn);

        //    var cmd = new OracleCommand("pck_flowbin_scale.checkloadedweight", conn)
        //    {
        //        CommandType = CommandType.StoredProcedure,
        //        BindByName = true
        //    };
        //    OracleParameter cp = new OracleParameter
        //    {
        //        ParameterName = "p_weight",
        //        Size = 200,
        //        OracleDbType = OracleDbType.Int32,
        //        Direction = ParameterDirection.Input,
        //        Value = loadedweight
        //    };
        //    cmd.Parameters.Add(cp);
        //    OracleParameter cp2 = new OracleParameter
        //    {
        //        ParameterName = "p_station_code",
        //        Size = 200,
        //        OracleDbType = OracleDbType.Varchar2,
        //        Direction = ParameterDirection.Input,
        //        Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
        //    };
        //    cmd.Parameters.Add(cp2);

        //    OracleParameter cp3 = new OracleParameter
        //    {
        //        ParameterName = "result",
        //        OracleDbType = OracleDbType.Int32,
        //        Direction = ParameterDirection.ReturnValue,
        //        Size = 200
        //    };

        //    cmd.Parameters.Add(cp3);

        //    cmd.Connection.Open();
        //    cmd.ExecuteScalar();

        //   if( Convert.ToInt32(cmd.Parameters["result"].Value.ToString())==1)
        //        check = true;

        //    cmd.Connection.Close();
        //    cmd.Dispose();

        //    return check;
        //}

        ////  :result := pck_flowbin_scale.checkunloadedweight(p_weight => :p_weight,p_station_code => :p_station_code);
        //public int CheckUnLoadedWeight(int Unloadedweight)
        //{
        //    conn = new OracleConnection(_oraConn);

        //    var cmd = new OracleCommand("pck_flowbin_scale.checkunloadedweight", conn)
        //    {
        //        CommandType = CommandType.StoredProcedure,
        //        BindByName = true
        //    };
        //    OracleParameter cp = new OracleParameter
        //    {
        //        ParameterName = "p_weight",
        //        Size = 200,
        //        OracleDbType = OracleDbType.Int32,
        //        Direction = ParameterDirection.Input,
        //        Value = Unloadedweight
        //    };
        //    cmd.Parameters.Add(cp);
        //    OracleParameter cp2 = new OracleParameter
        //    {
        //        ParameterName = "p_station_code",
        //        Size = 200,
        //        OracleDbType = OracleDbType.Varchar2,
        //        Direction = ParameterDirection.Input,
        //        Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
        //    };
        //    cmd.Parameters.Add(cp2);

        //    OracleParameter cp3 = new OracleParameter
        //    {
        //        ParameterName = "result",
        //        OracleDbType = OracleDbType.Int32,
        //        Direction = ParameterDirection.ReturnValue,
        //        Size = 200
        //    };

        //    cmd.Parameters.Add(cp3);

        //    cmd.Connection.Open();
        //    cmd.ExecuteScalar();

        //    int returnValue = Convert.ToInt32(cmd.Parameters["result"].Value.ToString());

        //    cmd.Connection.Close();
        //    cmd.Dispose();

        //    return returnValue;
        //}

        ////          :result := pck_flowbin_scale.checkliterrange(p_liter => :p_liter,
        ////                                       p_station_code => :p_station_code);

        //public Boolean CheckLiterRange(Decimal Liter)
        //{
        //    Boolean check = false;
        //    conn = new OracleConnection(_oraConn);

        //    var cmd = new OracleCommand("pck_flowbin_scale.checkliterrange", conn)
        //    {
        //        CommandType = CommandType.StoredProcedure,
        //        BindByName = true
        //    };
        //    OracleParameter cp = new OracleParameter
        //    {
        //        ParameterName = "p_liter",
        //        Size = 200,
        //        OracleDbType = OracleDbType.Decimal,
        //        Direction = ParameterDirection.Input,
        //        Value = Liter
        //    };
        //    cmd.Parameters.Add(cp);
        //    OracleParameter cp2 = new OracleParameter
        //    {
        //        ParameterName = "p_station_code",
        //        Size = 200,
        //        OracleDbType = OracleDbType.Varchar2,
        //        Direction = ParameterDirection.Input,
        //        Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
        //    };
        //    cmd.Parameters.Add(cp2);

        //    OracleParameter cp3 = new OracleParameter
        //    {
        //        ParameterName = "result",
        //        OracleDbType = OracleDbType.Int32,
        //        Direction = ParameterDirection.ReturnValue,
        //        Size = 200
        //    };

        //    cmd.Parameters.Add(cp3);

        //    cmd.Connection.Open();
        //    cmd.ExecuteScalar();

        //    if (Convert.ToInt32(cmd.Parameters["result"].Value.ToString()) == 1)
        //        check = true;

        //    cmd.Connection.Close();
        //    cmd.Dispose();

        //    return check;
        //}
        #endregion

        #region Scan Report Details For DataGrid         
        // Get Details Of Bins Scanned From TBL_PTS_POWDER_RECOVERY
        public DataTable ScannedBinsForYesterday()
        {
            var dt = new DataTable();
            var conn = new OracleConnection(_oraConn);

            conn.Open();
            var cmd = new OracleCommand("pck_flowbin_Capture.ScannedFBinCapForYesterday", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;


            OracleParameter cp2 = new OracleParameter();
            cp2.ParameterName = "p_StationCode";
            cp2.Size = 200;
            cp2.OracleDbType = OracleDbType.Varchar2;
            cp2.Direction = ParameterDirection.Input;
            cp2.Value = Properties.Settings.Default.ScanWorkStation.Trim().ToUpper();

            cmd.Parameters.Add(cp2);


            OracleParameter cp1 = new OracleParameter();
            cp1.ParameterName = "pCursor";
            cp1.OracleDbType = OracleDbType.RefCursor;
            cp1.Direction = ParameterDirection.Output;
            cp2.Size = 200;
            cmd.Parameters.Add(cp1);
                        
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }
            cmd.Connection.Close();
            cmd.Dispose();
            return dt;
        }
        public DataTable ScannedBinsForShift()
        {
            var dt = new DataTable();
            var conn = new OracleConnection(_oraConn);

            conn.Open();
            var cmd = new OracleCommand("pck_flowbin_Capture.ScannedFBinCapForShift", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;


            OracleParameter cp2 = new OracleParameter();
            cp2.ParameterName = "p_StationCode";
            cp2.Size = 200;
            cp2.OracleDbType = OracleDbType.Varchar2;
            cp2.Direction = ParameterDirection.Input;
            cp2.Value = Properties.Settings.Default.ScanWorkStation.Trim().ToUpper(); //test sapid Bin "WT24S003A6";

            cmd.Parameters.Add(cp2);


            OracleParameter cp1 = new OracleParameter();
            cp1.ParameterName = "pCursor";
            cp1.OracleDbType = OracleDbType.RefCursor;
            cp1.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(cp1);

            // Execute command
            //OracleDataReader reader;
            //reader = 
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }
            cmd.Connection.Close();
            cmd.Dispose();

            return dt;

            //    var count = reader.FieldCount;

            //    while (reader.Read())
            //    {
            //        //   return reader.GetString(0) + "  "+ reader.GetString(1) + "  " + reader.GetValue(2).ToString() + " " +count.ToString()  ;
            //        return reader["roll_no"].ToString() + " " + reader["grade_code"].ToString() + " " + reader["gsm_target"].ToString();
            //    }

            //    return "null";
            //}
        }
        public DataTable ScannedBinsForTheDay()
        {
            var dt = new DataTable();
            var conn = new OracleConnection(_oraConn);

            conn.Open();
            var cmd = new OracleCommand("pck_flowbin_Capture.ScannedF" +
                "" +
                "" +
                "" +
                "BinCapForTheDay", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;


            OracleParameter cp2 = new OracleParameter();
            cp2.ParameterName = "p_StationCode";
            cp2.Size = 200;
            cp2.OracleDbType = OracleDbType.Varchar2;
            cp2.Direction = ParameterDirection.Input;
            cp2.Value = Properties.Settings.Default.ScanWorkStation.Trim().ToUpper(); //test sapid Bin "WT24S003A6";

            cmd.Parameters.Add(cp2);


            OracleParameter cp1 = new OracleParameter();
            cp1.ParameterName = "pCursor";
            cp1.OracleDbType = OracleDbType.RefCursor;
            cp1.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(cp1);

            // Execute command
            //OracleDataReader reader;
            //reader = 
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            cmd.Connection.Close();
            cmd.Dispose();

            return dt;

            //    var count = reader.FieldCount;

            //    while (reader.Read())
            //    {
            //        //   return reader.GetString(0) + "  "+ reader.GetString(1) + "  " + reader.GetValue(2).ToString() + " " +count.ToString()  ;
            //        return reader["roll_no"].ToString() + " " + reader["grade_code"].ToString() + " " + reader["gsm_target"].ToString();
            //    }

            //    return "null";
            //}
        }  
        public string GetCurrentShift()
        {
            var dt = new DataTable();
            var conn = new OracleConnection(_oraConn);


            var cmd = new OracleCommand("pck_flowbin_Capture.GetCurrentShift", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;


            OracleParameter cp1 = new OracleParameter();
            cp1.ParameterName = "shiftcode";
            cp1.OracleDbType = OracleDbType.Varchar2;
            cp1.Direction = ParameterDirection.Output;
            cp1.Size = 20;
            cmd.Parameters.Add(cp1);


            cmd.Connection.Open();
            cmd.ExecuteScalar();

            string returnValue = "Shift     " + cmd.Parameters["shiftCode"].Value.ToString();

            cmd.Connection.Close();
            cmd.Dispose();

            return returnValue;
        }
        #endregion

        #region Get label data for printing
        public string GetLabelData()
        {
            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("pck_label_flowbin.fnc_label", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter();
            cp.ParameterName = "istrBatch";
            cp.Size = 200;
            cp.OracleDbType = OracleDbType.Varchar2;
            cp.Direction = ParameterDirection.Input;
            cp.Value = _sapBinId.Trim().ToUpper();
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter();
            cp1.ParameterName = "istrStation";
            cp1.Size = 200;
            cp1.OracleDbType = OracleDbType.Varchar2;
            cp1.Direction = ParameterDirection.Input;
            cp1.Value = Properties.Settings.Default.ScanWorkStation.Trim().ToUpper();
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter();
            cp2.ParameterName = "result";
            cp2.OracleDbType = OracleDbType.Varchar2;
            cp2.Direction = ParameterDirection.ReturnValue;
            cp2.Size = 5000;
            cmd.Parameters.Add(cp2);

            cmd.Connection.Open();
            cmd.ExecuteScalar();

            string returnValue = cmd.Parameters["result"].Value.ToString();

            cmd.Connection.Close();
            cmd.Dispose();

            return returnValue;
        }
        #endregion

        #region Get Printer Brand
        public string GetPrinterBrand()
        {
            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("pck_label_flowbin.GetPrinterBrand", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp1 = new OracleParameter();
            cp1.ParameterName = "istrStation";
            cp1.Size = 200;
            cp1.OracleDbType = OracleDbType.Varchar2;
            cp1.Direction = ParameterDirection.Input;
            cp1.Value = Properties.Settings.Default.ScanWorkStation.Trim().ToUpper();
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter();
            cp2.ParameterName = "result";
            cp2.OracleDbType = OracleDbType.Varchar2;
            cp2.Direction = ParameterDirection.ReturnValue;
            cp2.Size = 5000;
            cmd.Parameters.Add(cp2);

            cmd.Connection.Open();
            cmd.ExecuteScalar();

            string returnValue = cmd.Parameters["result"].Value.ToString();

            cmd.Connection.Close();
            cmd.Dispose();

            return returnValue;
        }
        #endregion

        #region Scale AuthBox 

        public int AuthCheck()
        {
            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("pck_flowbin_scale.checkusernamepassword", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_username",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = this._username.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_password",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = this._password.Trim()
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_empnum",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = this._empno.Trim()
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };

            cmd.Parameters.Add(cp3);

            cmd.Connection.Open();
            cmd.ExecuteScalar();

            int returnValue = Convert.ToInt32(cmd.Parameters["result"].Value.ToString());

            cmd.Connection.Close();
            cmd.Dispose();

            return returnValue;
        }

        //  pck_flowbin_scale.updatemilltransactionslog(p_param_code => :p_param_code,
        //                                              comment1 => :comment1,
        //                                              p_station_code => :p_station_code,
        //                                              p_empno => :p_empno,
        //                                              p_username => :p_username,
        //                                              p_reason => :p_reason);

        public void Authlog(string param_code, string empno, string username, string reason)

        {
            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("pck_flowbin_scale.updatemilltransactionslog", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_param_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = param_code.Trim().ToUpper()
            };

            cmd.Parameters.Add(cp);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "comment1",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = reason.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "p_station_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "p_empno",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = empno.Trim()
            };
            cmd.Parameters.Add(cp4);
              
            OracleParameter cp5 = new OracleParameter
            {
                ParameterName = "p_username",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = username.Trim().ToUpper()
            };
             cmd.Parameters.Add(cp5);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "p_reason",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = reason.Trim().ToUpper()
            };
               cmd.Parameters.Add(cp6);
       
              cmd.Connection.Open();
              cmd.ExecuteScalar();
              cmd.Connection.Close();
              cmd.Dispose();
        }





        //:result := pck_flowbin_scale.checkscalestates(p_param_code => :p_param_code,
        //                                        p_station_code => :p_station_code);
    public Boolean ScaleActiveStates(string ParamCode)
    {
            Boolean check = false;
        conn = new OracleConnection(_oraConn);

        var cmd = new OracleCommand("pck_flowbin_scale.checkscalestates", conn);

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_param_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = ParamCode.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
            ParameterName = "p_station_code",
            Size = 200,
            OracleDbType = OracleDbType.Varchar2,
            Direction = ParameterDirection.Input,
            Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp1);

           OracleParameter cp2 = new OracleParameter
           {
            ParameterName = "result",
            OracleDbType = OracleDbType.Varchar2,
            Direction = ParameterDirection.ReturnValue,
            Size = 200
           };
           cmd.Parameters.Add(cp2);

        cmd.Connection.Open();
        cmd.ExecuteScalar();

            if (Convert.ToInt32(cmd.Parameters["result"].Value.ToString()) == 1)
                check = true;
 
        cmd.Connection.Close();
        cmd.Dispose();

        return check;
        }

          //:result := pck_flowbin_scale.updatescaleactivestates(p_param_code => :p_param_code,
          //                                             p_station_code => :p_station_code);

        public Boolean UpdateScaleActiveStates(string ParamCode)
        {
            Boolean check = false;
            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("pck_flowbin_scale.updatescaleactivestates", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_param_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = ParamCode.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_station_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };
            cmd.Parameters.Add(cp2);

            cmd.Connection.Open();
            cmd.ExecuteScalar();

            if (Convert.ToInt32(cmd.Parameters["result"].Value.ToString()) == 1)
                check = true;

            cmd.Connection.Close();
            cmd.Dispose();

            return check;
        }

        //Get Flowbin Number
        //:result := pck_flowbin_scale.createflowbinnumber;
        public string GetFlowBinNumber()
    {
        conn = new OracleConnection(_oraConn);

        var cmd = new OracleCommand("pck_flowbin_scale.createflowbinnumber", conn)
        {
            CommandType = CommandType.StoredProcedure,
            BindByName = true
        };

        OracleParameter cp = new OracleParameter
        {
            ParameterName = "result",
            OracleDbType = OracleDbType.Varchar2,
            Direction = ParameterDirection.ReturnValue,
            Size = 200
        };

        cmd.Parameters.Add(cp);

        cmd.Connection.Open();
        cmd.ExecuteScalar();

        string returnValue = cmd.Parameters["result"].Value.ToString();

        cmd.Connection.Close();
        cmd.Dispose();

        return returnValue;
        }

      //  pck_flowbin_scale.validatescaleinputs(p_scale_value => :p_scale_value,
      //                                             p_param_code => :p_param_code,
       //                                            p_station_code => :p_station_code);
            public Boolean ValidateScaleInputs(int scaleinput ,string param_code)
        {
            Boolean check = false;
            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand(" pck_flowbin_scale.validatescaleinputs", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };
            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_scale_value",
                Size = 200,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = scaleinput
            };
            cmd.Parameters.Add(cp);
            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_param_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = param_code.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp1);
            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_station_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };

            cmd.Parameters.Add(cp3);

            cmd.Connection.Open();
            cmd.ExecuteScalar();

            if (Convert.ToInt32(cmd.Parameters["result"].Value.ToString()) == 1)
                check = true;

            cmd.Connection.Close();
            cmd.Dispose();

            return check;
        }

        #endregion

        #region Scale litres current state Enabled or Disabled
        public Boolean ScaleLiterseActiveState()
        {
           
            Boolean check =false;
            
            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand(" pck_flowbin_scale.checkscalestate", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_ScaleStationcode",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };

            cmd.Parameters.Add(cp1);

            cmd.Connection.Open();
            cmd.ExecuteScalar();

            if (Convert.ToInt32(cmd.Parameters["result"].Value.ToString())==1)
                check = true;
                
            
            cmd.Connection.Close();
            cmd.Dispose();

            return check;
        }

        #endregion

        #region Commit scale flowbin 
        public void RegisterScaleFlowBin(int LoadedWeight,int Unloadedweight,string FlowBinNo,decimal liters)
        {
            //pck_flowbin_scale.scalecommitflowbin(p_station_code => :p_station_code,
            //                               inummass => :inummass,
            //                               inummassunloaded => :inummassunloaded,
            //                               istrfbinnum => :istrfbinnum,
            //                               inlitres => :inlitres);

            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("pck_flowbin_scale.scalecommitflowbin", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };
           
            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_station_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "inummass",
                Size = 200,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = LoadedWeight
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "inummassunloaded",
                Size = 200,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = Unloadedweight
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "istrfbinnum",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = FlowBinNo
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "inlitres",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = liters
            };
            cmd.Parameters.Add(cp4);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }
        #endregion

        // Capture Details Getting LIS information to textboxes
        #region Get LIS data from MSSQL
        public DataTable LignoLIS_Data(int testid,int SamplingPointID, string TestDefName)
        {
            var dt = new DataTable();
            var conn1 = new SqlConnection(_sqlConn);
            
            conn1.Open();
            var cmd = new SqlCommand("[dbo].[Proc_Ligno_Liquid_FlowBins]", conn1)
            {
                CommandType = CommandType.StoredProcedure,
               
            };


            SqlParameter cp = new SqlParameter
            {
                ParameterName = "@p_testid",
                Size = 200,
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Input,
                Value = testid
            };

            cmd.Parameters.Add(cp);

            SqlParameter cp1 = new SqlParameter
            {
                ParameterName = "@p_SamplingPointID",
                Size = 200,
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Input,
                Value = SamplingPointID
            };

            cmd.Parameters.Add(cp1);

            SqlParameter cp2 = new SqlParameter
            {
                ParameterName = "@TestDefName",
                Size = 200,
                SqlDbType = SqlDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = TestDefName.Trim().ToUpper()
            };

            cmd.Parameters.Add(cp2);

            SqlParameter cp3 = new SqlParameter
            {
                ParameterName = "@SpecialTest",
                Size = 200,
                SqlDbType = SqlDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = ScaleActiveStates("SPTST") == true ? "True" : "False"
            };

            cmd.Parameters.Add(cp3);

            cmd.ExecuteReader();
            cmd.Connection.Close();
            

            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                da.Fill(dt);
            }
            cmd.Dispose();

            return dt;
            
        }
        #endregion

        // Capture Details for tracking
        #region Get Details from MSQL for tests
        public DataTable LignoLIS_DataList(int testid, int SamplingPointID, string TestDefName)
        {
            var dt = new DataTable();
            var conn1 = new SqlConnection(_sqlConn);

            conn1.Open();
            var cmd = new SqlCommand("[dbo].[Proc_Ligno_Liquid_FlowBins_datalist]", conn1)
            {
                CommandType = CommandType.StoredProcedure,

            };


            SqlParameter cp = new SqlParameter
            {
                ParameterName = "@p_testid",
                Size = 200,
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Input,
                Value = testid
            };

            cmd.Parameters.Add(cp);

            SqlParameter cp1 = new SqlParameter
            {
                ParameterName = "@p_SamplingPointID",
                Size = 200,
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Input,
                Value = SamplingPointID
            };

            cmd.Parameters.Add(cp1);

            SqlParameter cp2 = new SqlParameter
            {
                ParameterName = "@TestDefName",
                Size = 200,
                SqlDbType = SqlDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = TestDefName.Trim().ToUpper()
            };

            cmd.Parameters.Add(cp2);

            SqlParameter cp3 = new SqlParameter
            {
                ParameterName = "@SpecialTest",
                Size = 200,
                SqlDbType = SqlDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = ScaleActiveStates("SPTST") == true ? "True" : "False"
            };

            cmd.Parameters.Add(cp3);

            cmd.ExecuteReader();
            cmd.Connection.Close();


            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                da.Fill(dt);
            }
            cmd.Dispose();

            return dt;

        }
        #endregion

        public DataTable GetAvailableProducts()
        {
            conn = new OracleConnection(_oraConn);
            var dt = new DataTable();
            var cmd = new OracleCommand("pck_flowbin_capture.getavailablefbinproducts", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };

            cmd.Parameters.Add(cp);
            cmd.Connection.Open();
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            cmd.Connection.Close();
            cmd.Dispose();
            return dt;
        }
       
        public DataTable GetAvailableTanks()
    {
        conn = new OracleConnection(_oraConn);
        var dt = new DataTable();
        var cmd = new OracleCommand("pck_flowbin_capture.getgetavailabletanks", conn);

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };

            cmd.Parameters.Add(cp);
        cmd.Connection.Open();
        cmd.ExecuteReader();

        using (OracleDataAdapter da = new OracleDataAdapter(cmd))
        {
            da.Fill(dt);
        }

        cmd.Connection.Close();
        cmd.Dispose();
        return dt;
    }

        #region Get Test data from oracle to query Mssql LIS 
        public DataTable GetTestInformation(string samplepoint,int testid)
        {
          //:result:= pck_flowbin_capture.gettestinformation(p_samplepoint => :p_samplepoint,p_testid => :p_testid);

            conn = new OracleConnection(_oraConn);
            var dt = new DataTable();
            var cmd = new OracleCommand("pck_flowbin_capture.gettestinformation", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;


            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_samplepoint", //Samplepoint refs the location of test eg tank name
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = samplepoint.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_testid", //Samplepoint refs the location of test eg tank name
                Size = 200,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = testid
            };
            cmd.Parameters.Add(cp1);


            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };
            cmd.Parameters.Add(cp2);

            cmd.Connection.Open();
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            cmd.Connection.Close();
            cmd.Dispose();
            return dt;
        }
        #endregion

        #region Commit scan flow bin with test values
        public void RegisterScanFlowBin(string FlowBinNo, 
                                        decimal PH, 
                                        decimal Solids,
                                        decimal Insolubles,
                                        decimal Density,
                                        string Product,
                                        string GradeCode)
        {

            // pck_flowbin_capture.scancommitflowbin(p_station_code => :p_station_code,
            //                                 istrflowbinnum => :istrflowbinnum,
            //                                 p_ph => :p_ph,
            //                                 p_solids => :p_solids,
            //                                 p_insolubles => :p_insolubles,
            //                                 p_density => :p_density,
            //                                 p_product => :p_product,
            //                                 p_grade_code => :p_grade_code);

            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("pck_flowbin_capture.scancommitflowbin", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_station_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "istrflowbinnum",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = FlowBinNo.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_ph",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = PH
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "p_solids",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = Solids
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "p_insolubles",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = Insolubles
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp5 = new OracleParameter
            {
                ParameterName = "p_density",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = Density
            };
            cmd.Parameters.Add(cp5);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "p_product",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Product.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp6);

            //OracleParameter cp7 = new OracleParameter
            //{
            //    ParameterName = "p_grade_code",
            //    Size = 200,
            //    OracleDbType = OracleDbType.Varchar2,
            //    Direction = ParameterDirection.Input,
            //    Value = GradeCode.Trim().ToUpper()
            //};
           // cmd.Parameters.Add(cp7);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }


        #endregion

        #region Commit test results to tbl_test_results_flowbins

        //note some of the parameter are not need in manual but are used with automatic test.
        //manual get details from the test_flowbin table instead of LIS.
        public void RegisterFlowBinTestResults (string  FlowBinNo,
                                                string  result_id,
                                                string  lis_date,
                                                string  date_from,
                                                string  date_to,
                                                int     test_id,
                                                decimal result_act,
                                                decimal result_min,
                                                decimal result_max,
                                                decimal target,
                                                decimal result_lrl,
                                                decimal result_url,
                                                string  results_list,
                                                string  samplepoint)
        {
            //pck_flowbin_capture.registertestresults(p_flowbin_no => :p_flowbin_no,
            //                                  p_result_id => :p_result_id,
            //                                  p_lis_date => :p_lis_date,
            //                                  p_date_from => :p_date_from,
            //                                  p_date_to => :p_date_to,
            //                                  p_test_id => :p_test_id,
            //                                  p_result_act => :p_result_act,
            //                                  p_result_min => :p_result_min,
            //                                  p_result_max => :p_result_max,
            //                                  p_target => :p_target,
            //                                  p_result_lrl => :p_result_lrl,
            //                                  p_result_url => :p_result_url,
            //                                  p_results_list => :p_results_list,
            //                                  p_samplepoint => :p_samplepoint);

            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("pck_flowbin_capture.registertestresults", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_flowbin_no",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = FlowBinNo.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_result_id",
                Size = 800,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = result_id.Trim()
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_lis_date",
                Size = 800,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = lis_date.Trim()
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "p_date_from",
                Size = 800,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = date_from.Trim()
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "p_date_to",
                Size = 800,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = date_to.Trim()
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp5 = new OracleParameter
            {
                ParameterName = "p_test_id",
                Size = 800,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = test_id
            };
            cmd.Parameters.Add(cp5);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "p_result_act",
                Size = 800,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = result_act
            };
            cmd.Parameters.Add(cp6);

            OracleParameter cp7 = new OracleParameter
            {
                ParameterName = "p_result_min",
                Size = 800,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = result_min
            };
            cmd.Parameters.Add(cp7);

            OracleParameter cp8 = new OracleParameter
            {
                ParameterName = "p_result_max",
                Size = 800,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = result_max
            };
            cmd.Parameters.Add(cp8);

            OracleParameter cp9 = new OracleParameter
            {
                ParameterName = "p_target",
                Size = 800,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = target
            };
            cmd.Parameters.Add(cp9);

            OracleParameter cp10 = new OracleParameter
            {
                ParameterName = "p_result_lrl",
                Size = 800,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = result_lrl
            };
            cmd.Parameters.Add(cp10);

            OracleParameter cp11 = new OracleParameter
            {
                ParameterName = "p_result_url",
                Size = 800,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = result_url
            };
            cmd.Parameters.Add(cp11);

            OracleParameter cp12 = new OracleParameter
            {
                ParameterName = "p_results_list",
                Size = 800,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = results_list
            };
            cmd.Parameters.Add(cp12);

            OracleParameter cp13 = new OracleParameter
            {
                ParameterName = "p_samplepoint",
                Size = 800,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = samplepoint.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp13);


            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }

        #endregion

        #region Get scale settings
        public DataTable GetScaleParametersForSettings()
        {
           // :result:= PCK_FLOWBIN_SCALE.GetScaleParametersForSettings(p_station_code => :p_station_code);

            conn = new OracleConnection(_oraConn);
            var dt = new DataTable();
            var cmd = new OracleCommand("PCK_FLOWBIN_SCALE.GetScaleParametersForSettings", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_station_code", 
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };
            cmd.Parameters.Add(cp1);
            cmd.Connection.Open();
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            cmd.Connection.Close();
            cmd.Dispose();
            return dt;
        }
        #endregion

        public DataTable GetScaleParamSettingsRounding()
        {
            // :result:= PCK_FLOWBIN_SCALE.GetScaleParametersForSettings(p_station_code => :p_station_code);
            conn = new OracleConnection(_oraConn);
            var dt = new DataTable();
            var cmd = new OracleCommand("PCK_FLOWBIN_SCALE.GetScaleParamSettingsRounding", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_station_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };
            cmd.Parameters.Add(cp1);
            cmd.Connection.Open();
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            cmd.Connection.Close();
            cmd.Dispose();
            return dt;
        }

    
        public void UpdateParamSettings(string paramcode,
                                        int max,
                                        int min,
                                        int round)
        {
            //    PCK_FLOWBIN_SCALE.updateParamSettings(p_param_code => :p_param_code,
            //                                    p_min => :p_min,
            //                                    p_max => :p_max,
            //                                    p_round => :p_round,
            //                                    p_station_code => :p_station_code,
            //                                    p_empno => :p_empno,
            //                                    p_username => :p_username,
            //                                    p_reason => :p_reason);

            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("PCK_FLOWBIN_SCALE.updateParamSettings", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_param_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = paramcode.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_max",
                Size = 200,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = max
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_min",
                Size = 200,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = min
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "p_round",
                Size = 200,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = round
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "p_empno",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = _empno
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp5 = new OracleParameter
            {
                ParameterName = "p_username",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value =_username.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp5);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "p_reason",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = _reasondetails.Trim()
            };
            cmd.Parameters.Add(cp6);

            OracleParameter cp7 = new OracleParameter
            {
                ParameterName = "p_station_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp7);
            
            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }

        public DataTable GetInformtionForReprint(string flowbin_no,DateTime date)
        {
          //   :result:= PCK_FLOWBIN_CAPTURE.GetInformtionForReprint(p_flowbin_no => :p_flowbin_no,
          //                                              p_date => :p_date);
            conn = new OracleConnection(_oraConn);
            var dt = new DataTable();
            var cmd = new OracleCommand("PCK_FLOWBIN_CAPTURE.GetInformtionForReprint", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_flowbin_no",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = flowbin_no.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_date",
                Size = 200,
                OracleDbType = OracleDbType.Date,
                Direction = ParameterDirection.Input,
                Value = date
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };
            cmd.Parameters.Add(cp3);
            cmd.Connection.Open();
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            cmd.Connection.Close();
            cmd.Dispose();
            return dt;
        }

        public Boolean GetAuthBoxReprintState()
        {
            //:result := PCK_FLOWBIN_CAPTURE.GetAuthBoxReprintState(p_station_code => :p_station_code);

            Boolean check = false;
            
            conn = new OracleConnection(_oraConn);
            var dt = new DataTable();
            var cmd = new OracleCommand("PCK_FLOWBIN_CAPTURE.GetAuthBoxReprintState", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_station_code",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = Properties.Settings.Default.ScaleWorkStation.Trim().ToUpper()
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };

            cmd.Parameters.Add(cp1);
            cmd.Connection.Open();
            cmd.ExecuteReader();

            using (OracleDataAdapter da = new OracleDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            if (int.Parse(dt.Rows[0]["AuthBoxStateReprint"].ToString()) == 1)
                check = true;

            if (int.Parse(dt.Rows[0]["AuthBoxStateTank"].ToString()) == 1)
                _authboxstatetank = true;

            if (int.Parse(dt.Rows[0]["AuthBoxStateProduct"].ToString()) == 1)
                _authBoxstateproduct = true;

            cmd.Connection.Close();
            cmd.Dispose();
            return check ;
        }

        public Boolean CheckLignoLiquidSpecs(decimal specval,int testid,string samplepoint )
        {
         //:result:= PCK_FLOWBIN_CAPTURE.CheckLignoLiquidSpecs(p_spec_value => :p_spec_value,
         //                                                    p_test_id => :p_test_id,
         //                                                    p_samplepoint => :p_samplepoint);

            Boolean check = false;

            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("PCK_FLOWBIN_CAPTURE.CheckLignoLiquidSpecs", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_spec_value",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = specval
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_test_id",
                Size = 200,
                OracleDbType = OracleDbType.Int32,
                Direction = ParameterDirection.Input,
                Value = testid
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_samplepoint",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = samplepoint
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };

            cmd.Parameters.Add(cp3);

            cmd.Connection.Open();
            cmd.ExecuteScalar();

            if (Convert.ToInt32(cmd.Parameters["result"].Value.ToString()) == 1)
                check = true;


            cmd.Connection.Close();
            cmd.Dispose();

            return check;
        }

        public void UpdateReprint(string flowbinnumber)
        {
            //PCK_FLOWBIN_CAPTURE.UpdateReprint(p_flowbin_no => :p_flowbin_no,
            //                                    p_sap_no => :p_sap_no);
           
            conn = new OracleConnection(_oraConn);
            var cmd = new OracleCommand("PCK_FLOWBIN_CAPTURE.UpdateReprint", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };
            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_flowbin_no",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = flowbinnumber
            };
            cmd.Parameters.Add(cp);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }


       

        public void RegReasonForOutOfSpecTests(decimal ph,
                                               decimal solid,
                                               decimal insol,
                                               decimal density,
                                               string samplepoint,
                                               string flowbinno)
                                               //string username,
                                               //string empno,
                                               //string reason,
                                               //string flowbinno)
        {
            //PCK_FLOWBIN_CAPTURE.RegReasonForOutOfSpecTests(p_ph => :p_ph,
            //                                               p_sol => :p_sol,
            //                                               p_insol => :p_insol,
            //                                               p_den => :p_den,
            //                                               p_samplepoint => :p_samplepoint,
            //                                               p_username => :p_username,
            //                                               p_empno => :p_empno,
            //                                               p_reason => :p_reason,
            //                                               p_flowbinno => :p_flowbinno);

            conn = new OracleConnection(_oraConn);
            var cmd = new OracleCommand("PCK_FLOWBIN_CAPTURE.RegReasonForOutOfSpecTests", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };
            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_ph",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = ph
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_sol",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = solid
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_insol",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = insol
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "p_den",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = density
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "p_samplepoint",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = samplepoint
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp5 = new OracleParameter
            {
                ParameterName = "p_username",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = _username
            };
            cmd.Parameters.Add(cp5);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "p_empno",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = _empno
            };
            cmd.Parameters.Add(cp6);
            OracleParameter cp7 = new OracleParameter
            {
                ParameterName = "p_reason",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = _reasondetails
            };
            cmd.Parameters.Add(cp7);
            OracleParameter cp8 = new OracleParameter
            {
                ParameterName = "p_flowbinno",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = flowbinno
            };
            cmd.Parameters.Add(cp8);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }





        public void RegReasonForOutOfSpecProductTests(decimal ph,
                                               decimal solid,
                                               decimal insol,
                                               decimal density,
                                               string product,
                                               string flowbinno)
        
        {
            
             //PCK_FLOWBIN_CAPTURE.RegReasonForOutOfSpecProdTests(p_ph => :p_ph,
         //                                                   p_sol => :p_sol,
         //                                                   p_insol => :p_insol,
         //                                                   p_den => :p_den,
         //                                                   p_product => :p_product,
         //                                                   p_username => :p_username,
         //                                                   p_empno => :p_empno,
         //                                                   p_reason => :p_reason,
         //                                                   p_flowbinno => :p_flowbinno);

            conn = new OracleConnection(_oraConn);
            var cmd = new OracleCommand("PCK_FLOWBIN_CAPTURE.RegReasonForOutOfSpecProdTests", conn)
            {
                CommandType = CommandType.StoredProcedure,
                BindByName = true
            };
            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_ph",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = ph
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_sol",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = solid
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_insol",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = insol
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "p_den",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = density
            };
            cmd.Parameters.Add(cp3);

            OracleParameter cp4 = new OracleParameter
            {
                ParameterName = "p_product",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = product
            };
            cmd.Parameters.Add(cp4);

            OracleParameter cp5 = new OracleParameter
            {
                ParameterName = "p_username",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = _username
            };
            cmd.Parameters.Add(cp5);

            OracleParameter cp6 = new OracleParameter
            {
                ParameterName = "p_empno",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = _empno
            };
            cmd.Parameters.Add(cp6);
            OracleParameter cp7 = new OracleParameter
            {
                ParameterName = "p_reason",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = _reasondetails
            };
            cmd.Parameters.Add(cp7);
            OracleParameter cp8 = new OracleParameter
            {
                ParameterName = "p_flowbinno",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = flowbinno
            };
            cmd.Parameters.Add(cp8);

            cmd.Connection.Open();
            cmd.ExecuteScalar();
            cmd.Connection.Close();
            cmd.Dispose();
        }



        public Boolean CheckLignoProductSpecs(decimal testval, int testid, string productfullname)
        {
            // :result:= PCK_FLOWBIN_CAPTURE.IsProductInSpec(p_productfullname => :p_productfullname,
            //                                    p_testid => :p_testid,
            //                                    p_test_value => :p_test_value);

            Boolean check = false;

            conn = new OracleConnection(_oraConn);

            var cmd = new OracleCommand("PCK_FLOWBIN_CAPTURE.IsProductInSpec", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.BindByName = true;

            OracleParameter cp = new OracleParameter
            {
                ParameterName = "p_productfullname",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = productfullname
            };
            cmd.Parameters.Add(cp);

            OracleParameter cp1 = new OracleParameter
            {
                ParameterName = "p_testid",
                Size = 200,
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.Input,
                Value = testid
            };
            cmd.Parameters.Add(cp1);

            OracleParameter cp2 = new OracleParameter
            {
                ParameterName = "p_test_value",
                Size = 200,
                OracleDbType = OracleDbType.Decimal,
                Direction = ParameterDirection.Input,
                Value = testval
            };
            cmd.Parameters.Add(cp2);

            OracleParameter cp3 = new OracleParameter
            {
                ParameterName = "result",
                OracleDbType = OracleDbType.Varchar2,
                Direction = ParameterDirection.ReturnValue,
                Size = 200
            };
            cmd.Parameters.Add(cp3);

            cmd.Connection.Open();
            cmd.ExecuteScalar();

            if (Convert.ToInt32(cmd.Parameters["result"].Value.ToString()) == 1)
                check = true;

            cmd.Connection.Close();
            cmd.Dispose();

            return check;
        }

    }
}



