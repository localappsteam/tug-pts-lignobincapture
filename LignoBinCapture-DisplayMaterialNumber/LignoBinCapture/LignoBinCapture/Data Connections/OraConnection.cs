﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
namespace LignoBinCapture
{

   public class OraConnection 
    {

        public string OraConnectionString()
        {
            //var oraConnstring = new OracleConnectionStringBuilder
            //{

            //   DataSource= Properties.Settings.Default.DataSource,
            //   UserID =Properties.Settings.Default.UserID,
            //   Password= Properties.Settings.Default.Password
            //};

            // return oraConnstring.ConnectionString;
            if (Properties.Settings.Default.SwitchBtwLive_Dev==true)
             return Properties.Settings.Default.OraConn_Live;
            else
                return Properties.Settings.Default.OraConn_Dev;
        }

        public string MSSQLConnectionString()
        {
            var MSSQLConnstring = new SqlConnectionStringBuilder
            {
                ["server"] = Properties.Settings.Default.MSSQLServer,
                ["user id"]= Properties.Settings.Default.MSSQLUserID,
                ["password"]= Properties.Settings.Default.MSSQLPassword,
                ["initial catalog"]= Properties.Settings.Default.MSSQLCatalog,
                
            };
            MSSQLConnstring.MultipleActiveResultSets = true;
            //MSSQLConnstring.AsynchronousProcessing = true;

            return MSSQLConnstring.ConnectionString;
        }
    }
}
