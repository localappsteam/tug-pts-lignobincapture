﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LignoBinCapture.Scale_Settings_Form
{
    public partial class frmScaleSettings : Form
    {
        public frmScaleSettings()
        {
            InitializeComponent();
        }
        LignoBinCaptures lignobincaptures = new LignoBinCaptures();
        Boolean CheckLTR = false;
        Boolean CheckMRO = false;
        Boolean CheckEMLSB = false;
        Boolean CheckEMLUB = false;
        
        public Boolean CheckState { get; set; } = false;
        public DataTable updatedtable { get; set; }
        private void frmScaleSettings_Load(object sender, EventArgs e)
        {
            PopulateScaleRanges();
            PopulateScaleRounding();
          //  UpdatedParamSettings();
            ReSetChecks();
            
        }

        private void UpdateTableWithChanges()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("param_code", typeof(string));
            dt.Columns.Add("maxval", typeof(int));
            dt.Columns.Add("minval", typeof(int));
            dt.Columns.Add("Round", typeof(int));
            dt.Columns.Add("Checkval", typeof(Boolean));
            

            if (CheckEMLUB == true)
            // updatedtable=UpdatedParamSettings("EMLUB", 
            //                                   int.Parse( txtUnloadedMax.Text), 
            //                                   int.Parse(txtUnloadedMin.Text), 
            //                                   int.Parse("0"), 
            //                                   CheckEMLUB);
            {
                DataRow dr = dt.NewRow();
                dr["param_code"] = "EMLUB";
                dr["maxval"] = int.Parse(txtUnloadedMax.Text);
                dr["minval"] = txtUnloadedMin.Text;
                dr["Round"] = int.Parse("0");
                dr["Checkval"] = CheckEMLUB;
                dt.Rows.Add(dr);
            }

            if (CheckEMLSB == true)
            //updatedtable=UpdatedParamSettings("EMLSB", 
            //                                  int.Parse(txtLoadedMax.Text), 
            //                                  int.Parse(txtLoadedMin.Text), 
            //                                  int.Parse("0"), 
            //                                  CheckEMLSB);
            {
                DataRow dr = dt.NewRow();
                dr["param_code"] = "EMLSB";
                dr["maxval"] = int.Parse(txtLoadedMax.Text);
                dr["minval"] = int.Parse(txtLoadedMin.Text);
                dr["Round"] =0;
                dr["Checkval"] = CheckEMLSB;
               
                dt.Rows.Add(dr);
            }
            if (CheckLTR == true)
            //updatedtable = UpdatedParamSettings("LTR", 
            //                                    int.Parse(txtLtrMax.Text), 
            //                                    int.Parse(txtLtrMin.Text), 
            //                                    int.Parse("0"), 
            //                                    CheckLTR);
            {
                DataRow dr = dt.NewRow();
                dr["param_code"] = "LTR";
                dr["maxval"] = int.Parse(txtLtrMax.Text);
                dr["minval"] = int.Parse(txtLtrMin.Text);
                dr["Round"] = 0;
                dr["Checkval"] = CheckLTR;
                dt.Rows.Add(dr);
            }
            if (CheckMRO == true)
            //updatedtable = UpdatedParamSettings("MRO", 
            //                                    int.Parse(txtRoundMax.Text),
            //                                    int.Parse(txtRoundMin.Text), 
            //                                    int.Parse(txtRoundTO.Text), 
            //                                    CheckMRO);
            {
                DataRow dr = dt.NewRow();
                dr["param_code"] = "MRO";
                dr["maxval"] = int.Parse(txtRoundMax.Text);
                dr["minval"] = int.Parse(txtRoundMin.Text);
                dr["Round"] = int.Parse(txtRoundTO.Text);
                dr["Checkval"] = CheckMRO;
                dt.Rows.Add(dr);
            }

            updatedtable = dt;
        }
        private void ReSetChecks()
        {
            CheckLTR = false;
            CheckMRO = false;
            CheckEMLSB = false;
            CheckEMLUB = false;
        }

        private DataTable UpdatedParamSettings(string param,int max,int min,int round,Boolean check)
        {
          
                DataTable dt = new DataTable();
            dt.Columns.Add("param_code", typeof(string));
            dt.Columns.Add("maxval", typeof(int));
            dt.Columns.Add("minval", typeof(int));
            dt.Columns.Add("Round", typeof(int));
            dt.Columns.Add("Checkval", typeof(Boolean));

            DataRow dr = dt.NewRow();
            dr["param_code"] = param;
            dr["maxval"] = max;
            dr["minval"] = min;
            dr["Round"] = round;
            dr["Checkval"] =check;
            dt.Rows.Add(dr);

            return dt;
        }

        private void PopulateScaleRounding()
        {

            foreach (DataRow dt in lignobincaptures.GetScaleParamSettingsRounding().Rows)
            {

                switch (dt["param_code"].ToString())
                {
                    case "MRO":
                        txtRoundMax.Text = dt["Max_val"].ToString();
                        txtRoundMin.Text = dt["min_val"].ToString();
                        txtRoundTO.Text = dt["round_to"].ToString();

                        break;
                    default:
                        break;
                }
            }
        }
        private void PopulateScaleRanges()
        {
            foreach (DataRow dt in lignobincaptures.GetScaleParametersForSettings().Rows)
            {

                switch (dt["param_code"].ToString())
                {
                    case "LTR":
                        txtLtrMax.Text = dt["Maxval"].ToString();
                        txtLtrMin.Text = dt["Minval"].ToString();
                        break;

                    case "EMLUB":
                        txtUnloadedMax.Text = dt["Maxval"].ToString();
                        txtUnloadedMin.Text = dt["Minval"].ToString();
                        break;

                    case "EMLSB":
                        txtLoadedMax.Text = dt["Maxval"].ToString();
                        txtLoadedMin.Text = dt["Minval"].ToString();
                        break;

                    default:
                        break;

                }
            }
        }
        private Boolean CheckRangeInput(string range)
        {
            Boolean Check = false;
            if (!Int32.TryParse(range, out int j))
            {
                MessageBox.Show("Value not a number");

                Check = false;
            }
            else
                Check = true;

            return Check;
        }
        private void txtUnloadedMax_TextChanged(object sender, EventArgs e)
        {
            if (CheckRangeInput(txtUnloadedMax.Text) == true)
                CheckEMLUB = true;
            else
                txtUnloadedMax.Text = "0";
        }
        private void txtUnloadedMin_TextChanged(object sender, EventArgs e)
        {
            if (CheckRangeInput(txtUnloadedMin.Text) == true)
                CheckEMLUB = true;
            else
                txtUnloadedMin.Text = "0";
        }
        private void txtLoadedMax_TextChanged(object sender, EventArgs e)
        {
            if (CheckRangeInput(txtLoadedMax.Text) == true)
                CheckEMLSB = true;
            else
                txtLoadedMax.Text = "0";
        }
        private void txtLoadedMin_TextChanged(object sender, EventArgs e)
        {
            if (CheckRangeInput(txtLoadedMin.Text) == true)
                CheckEMLSB = true;
            else
                txtLoadedMin.Text = "0";
        }
        private void txtLtrMax_TextChanged(object sender, EventArgs e)
        {

            if (CheckRangeInput(txtLtrMax.Text) == true)
                CheckLTR = true;
            else
                txtLtrMax.Text = "0";
        }
        private void txtLtrMin_TextChanged(object sender, EventArgs e)
        {
            if (CheckRangeInput(txtLtrMin.Text) == true)
                CheckLTR = true;
            else
                txtLtrMin.Text = "0";
        }
        private void txtRoundMax_TextChanged(object sender, EventArgs e)
        {
            if (CheckRangeInput(txtRoundMax.Text) == true)
                CheckMRO = true;
            else
                txtRoundMax.Text = "0";
        }
        private void txtRoundMin_TextChanged(object sender, EventArgs e)
        {
            if (CheckRangeInput(txtRoundMin.Text) == true)
                CheckMRO = true;
            else
                txtRoundMin.Text = "0";
        }
        private void txtRoundTO_TextChanged(object sender, EventArgs e)
        {
            if (CheckRangeInput(txtRoundTO.Text) == true)
                CheckMRO = true;
            else
                txtRoundTO.Text = "0";
        }
        private void btnOk_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Are you sure you want to SAVE CHANGES", "Add",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                UpdateTableWithChanges();
                CheckState = true;
                Close();
            }
           
               
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CheckState = false;
            Close();
        }

        
    }
}

