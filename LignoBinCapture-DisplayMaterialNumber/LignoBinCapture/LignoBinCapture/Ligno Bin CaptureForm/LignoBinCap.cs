﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Xsl;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Text;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace LignoBinCapture
{
    public partial class LignoBinCap : Form
    {
        public LignoBinCap()
        {
            InitializeComponent();
        }

        #region Variables Error Messages and Constants
        const string _msg_BatchProcesssed = "\nFlow Bin : {0} Has Been Processed.\n";
        const string _msg_BatchNotProcesssed = "\nFlow Bin : {0} Unable To Process.\n";
        const string _msg_BinVaildForProcessing = "\nFlow Bin : {0} Bin Valid For Processing.\n";
        const string _msg_saved = "Scale Settings\nSAVED";
        const string _error_problem_saving = "Problem \nSaving Scale Settings \n";
        const string _error_MsgBox = "\nError Has Occurred\n-TNS Names Error-\nContact Dev on ext 1153.\n";
        const string _error_Printing = "\nBatch Number : {0} : Error connecting to printer.\n";
        const string _error_TextFile = "\nBatch Number : {0} : Error creating Label.txt file.\n";
        const string _error_Transform = "\nBatch Number : {0} : XSLT transformation error.\n";
        const string _error_Register = "\nBatch Number : {0} : Unable to register Flow Bin Label.\n";
        const string _error_RegisterPrinter = "\nBatch Number : {0} : Printer unreachable.Unable To Print Label\n";
        const string _msg_OutOfSpec = "Ligno Liquid Specifications NOT IN Range\n\nSelect:\n\nYES : Approve \n\nNO : Select Another Tank/Product ";
        const string _error_Weight = "NOT VALID";
        const string _Weight_Capture_sucessfull = "\nVALID\n";

        private string _dateToday = ("  " + "Today     " + DateTime.Now.ToString("dd/MM/yyyy")).ToUpper();
        private string _dateYesterday = ("  " + "Yesterday     " + DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy")).ToUpper();

        public const string scaleCodeState = "SMOF";
        public const string scaleCodeMRO = "MRO";
        public const string scaleCodeLtr = "LTR";
        public const string scaleCodeUnloadedCheck = "EMLUB";
        public const string scaleCodeLoadedCheck = "EMLSB";
        public const string AuthBoxCodeCheck = "RPAUTH";
        public const string scaleNetMasscalcCode = "SUBTU";
        public const string specialTestCode = "SPTST";
       public const string ManualTestCapture = "MANC";
      //public const string manualTestInputCode = "MTSTI";
        #endregion

        #region Variables Declaration

        // Global properties
        LignoBinCaptures lignoBinCaptures = new LignoBinCaptures();
        SerialPortManager _spManager = new SerialPortManager();

        public DataTable ProcessUpdatedScaleSettings;
        public string flowbinnumber
        {
            get { return lignoBinCaptures.SapBinId; }
            set { lignoBinCaptures.SapBinId = value; }
        }
        public Boolean ReasonboxExitCheck { get; set; } = true;

        //Local Variables
        private string logviewer = Properties.Settings.Default.LogFileViewer;
        private string errorlogfile = Properties.Settings.Default.ErrorLogPath;
        private string scalestationcode = Properties.Settings.Default.ScaleWorkStation;
        private Boolean cmbTanksLoaded = true;
        private Boolean cmbProductsLoaded = true;
        private Boolean FlowBinRegistered = true;
        private Boolean lisdataloaded = false;
        private Boolean CheckLiters = false;
        private int ScaleWeighOrder = 0;
        //Auth Box Return info to log with auth log table
        private string Username;
        private string EmpNo;
        private string Reason;
        private Boolean AuthSuccess = false;
        private string GradeCode;
        //Range Values for scale
        private string RangeLtrMin;
        private string RangeLtrMax;
        private string RangeUnloadedMin;
        private string RangeUnloadedMax;
        private string RangeLoadedMin;
        private string RangeLoadedMax;
        #region Getting Test Results for capture
        private string ResultsID_PH = ""; //Used to concat all the values from lis into one string for tracking info
        private string DateFrom_PH = ""; //Used to concat all the values from lis into one string for tracking info
        private string DateTo_PH = ""; //Used to concat all the values from lis into one string for tracking info
        private string ResultsTimeStamp_PH = ""; //Used to concat all the values from lis into one string for tracking info
        private string TestValueResults_List_PH = ""; //Used to concat all the values from lis into one string for tracking info
        //Getting Test Results for capture -Solids
        private string ResultsID_Solids = ""; //Used to concat all the values from lis into one string for tracking info
        private string DateFrom_Solids = ""; //Used to concat all the values from lis into one string for tracking info
        private string DateTo_Solids = ""; //Used to concat all the values from lis into one string for tracking info
        private string ResultsTimeStamp_Solids = ""; //Used to concat all the values from lis into one string for tracking info
        private string TestValueResults_List_Solids = ""; //Used to concat all the values from lis into one string for tracking info
        //Getting Test Results for capture -Insolubles
        private string ResultsID_Insolubles = ""; //Used to concat all the values from lis into one string for tracking info
        private string DateFrom_Insolubles = ""; //Used to concat all the values from lis into one string for tracking info
        private string DateTo_Insolubles = ""; //Used to concat all the values from lis into one string for tracking info
        private string ResultsTimeStamp_Insolubles = ""; //Used to concat all the values from lis into one string for tracking info
        private string TestValueResults_List_Insolubles = ""; //Used to concat all the values from lis into one string for tracking info
        //Getting Test Results for capture Density
        private string ResultsID_Density = ""; //Used to concat all the values from lis into one string for tracking info
        private string DateFrom_Density = ""; //Used to concat all the values from lis into one string for tracking info
        private string DateTo_Density = ""; //Used to concat all the values from lis into one string for tracking info
        private string ResultsTimeStamp_Density = ""; //Used to concat all the values from lis into one string for tracking info
        private string TestValueResults_List_Density = ""; //Used to concat all the values from lis into one string for tracking info
        #endregion
        #endregion

        #region Commit FlowBin to Tempinv,Pts_Transactions,H_Flowbin and Test_results_flowbins
        private void Register()
        {
            Boolean check = false;
            try
            {
                if (lisdataloaded)

                {
                    lignoBinCaptures.SapBinId = lblFlowBinNumber.Text;

                    lignoBinCaptures.RegisterScaleFlowBin(Convert.ToInt32(txtLoaded.Text),
                                                          Convert.ToInt32(txtUnloaded.Text),
                                                                          lblFlowBinNumber.Text,
                                                          Convert.ToDecimal(txtLiters.Text));

                    lignoBinCaptures.RegisterScanFlowBin(lblFlowBinNumber.Text,
                                                    Convert.ToDecimal(txtPh.Text),
                                                    Convert.ToDecimal(txtSolids.Text),
                                                    Convert.ToDecimal(txtInsolubles.Text),
                                                    Convert.ToDecimal(txtDensity.Text),
                                                                      cmbProduct.Text, "");
                  //  if (chkManualCapture.Checked == false)
                   // {
                        for (int Testno = 1; Testno < 5; Testno++)
                        RegisterFlowBinTestResults(lblFlowBinNumber.Text, Testno, cmbTanks.Text);
                     
                   // }
                    //Register Tank Out of spec tests in TBL_tempinv,TBL_Pts_Transactions and TBL_H_flowbins 
                    if (CheckLignoLiquidSpecs())
                    {
                        if (lignoBinCaptures.AuthBoxStateTank)
                            lignoBinCaptures.RegReasonForOutOfSpecTests(Convert.ToDecimal(txtPh.Text),
                                                                        Convert.ToDecimal(txtSolids.Text),
                                                                        Convert.ToDecimal(txtInsolubles.Text),
                                                                        Convert.ToDecimal(txtDensity.Text),
                                                                                          cmbTanks.Text,
                                                                                          lblFlowBinNumber.Text);
                    }
                    //Register Product Out of spec tests in TBL_tempinv,TBL_Pts_Transactions and TBL_H_flowbins
                    if (CheckLignoProductSpecs())
                    {
                        if (lignoBinCaptures.AuthBoxStateProduct)
                            lignoBinCaptures.RegReasonForOutOfSpecProductTests(Convert.ToDecimal(txtPh.Text),
                                                                        Convert.ToDecimal(txtSolids.Text),
                                                                        Convert.ToDecimal(txtInsolubles.Text),
                                                                        Convert.ToDecimal(txtDensity.Text),
                                                                                          cmbProduct.Text,
                                                                                          lblFlowBinNumber.Text);
                    }

                    error_Label.ForeColor = Color.Green;
                    error_Label.Text = string.Format(_msg_BatchProcesssed, lblFlowBinNumber.Text);
                    check = true;
                }
                #region Unused code
                //else
                //{
                //TO Do addtional error Trapping here 
                //MessageBox.Show("" + cmbTanks.Text);

                //}
                #endregion
                if (check)
                {
                    PrintSAPLabel();
                    Reset_Everything();
                }
            }

            catch (Exception ex)
            {
                FlowBinRegistered = false;
                error_Label.ForeColor = Color.Crimson;
                error_Label.Text = string.Format(_error_Register, lignoBinCaptures.SapBinId) + "\n" + ex.Message + " " + ex.Data;
                MessageBox.Show(string.Format(_error_Register, lignoBinCaptures.SapBinId) + "\n" +
                                ex.Message, "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

                Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                   Properties.Settings.Default.ErrorLogloc,
                                   DateTime.Now +
                                   "Error " + string.Format(_error_Register, lignoBinCaptures.SapBinId) +
                                   "\n" +
                                   " --> " +
                                   ex.Message +
                                   " " +
                                   ex.TargetSite + " " + ex.InnerException);
                Reset_Everything();
            }
        }

        #endregion

        #region Populate Product ComboBox 
        public void PopulateTanksComboBox()
        {
            cmbTanksLoaded = false;
            cmbTanks.DataSource = null;
            cmbTanks.ValueMember = "SAMPLEPOINT";
            cmbTanks.DisplayMember = "SAMPLEPOINT";
            cmbTanks.DataSource = lignoBinCaptures.GetAvailableTanks();
            cmbTanks.SelectedValue.ToString();
            cmbTanksLoaded = true;
            cmbTanks.Text = "Select Tank ...";
        }
        public void PopulateProductComboBox()
        {

            cmbProductsLoaded = false;
            cmbProduct.DataSource = null;
            cmbProduct.ValueMember = "NAME_FULL";
            cmbProduct.DisplayMember = "NAME_FULL";
            cmbProduct.DataSource = lignoBinCaptures.GetAvailableProducts();
            cmbProduct.SelectedValue.ToString();
            cmbProductsLoaded = true;
            cmbProduct.Text = "Select Product ...";

        }
        #endregion

        #region Reset Application Controls
        private void bnt_Clear_Click(object sender, EventArgs e)
        {
            try
            {
                Reset_Everything();
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR CLEARING UP \n" +
                                ex.Message,
                                "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

                Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                   Properties.Settings.Default.ErrorLogloc,
                                   DateTime.Now +
                                   " --> " + "ERROR" +
                                   ex.Message +
                                   " " +
                                   ex.TargetSite);

            }
        }

        private void bnt_Exit_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region Call Bins Scanned Report Details to DataGridView
        private void CallBinsScannedReportdetails()
        {
            BinsScannedReport(dataGrid_Scanned, lignoBinCaptures.ScannedBinsForTheDay(), _dateToday, lbl_Today);

            BinsScannedReport(dataGrid_Yesterday, lignoBinCaptures.ScannedBinsForYesterday(), _dateYesterday, lbl_Yesterday);

            BinsScannedReport(dataGrid_Shift, lignoBinCaptures.ScannedBinsForShift(), lignoBinCaptures.GetCurrentShift(), lbl_Shift);

        }

        #endregion

        #region On application StartUp Apply settings
        private void ScaleStart()
        {
            lignoBinCaptures.GetAuthBoxReprintState();
            bnt_ProcessBin.Enabled = false;
            Reset_Everything();
            PopulateProductComboBox();
            PopulateTanksComboBox();
            CallBinsScannedReportdetails();

            tslbl_StationCode.Text = "Scan :" +
                                     Properties.Settings.Default.ScanWorkStation +
                                     " " +
                                     "Scale :" +
                                     Properties.Settings.Default.ScaleWorkStation;
            SetControlStateBasedOnParam();
        }

        private void SetControlStateBasedOnParam()
        {
            ScaleInputState(scaleCodeState);
            ScaleInputState(scaleCodeMRO);
            ScaleInputState(scaleCodeLtr);
            ScaleInputState(scaleNetMasscalcCode);
            ScaleInputState(specialTestCode);
            ScaleInputState(ManualTestCapture);
          // ScaleInputState(manualTestInputCode);
        }
        #endregion

        private void checkIfDbIsDevOrLive()
        {
            if (Properties.Settings.Default.SwitchBtwLive_Dev == true)
            {
                tsslLiveOrDev.BackColor = Color.Green;
                tsslLiveOrDev.ForeColor = Color.White;
                tsslLiveOrDev.Text = "Working On LIVE";
            }
            else
            {
                tsslLiveOrDev.BackColor = Color.Red;
                tsslLiveOrDev.ForeColor = Color.White;
                tsslLiveOrDev.Text = "Working On DEV : " + Properties.Settings.Default.OraConn_Dev;
            }
        }
        #region load defaults at startup
        private void LignoLignoBinCap_Load(object sender, EventArgs e)
        {

            try
            {
                checkIfDbIsDevOrLive();
               // EnableForManualTestCapture();
                UserInitialization();
                _spManager.StartListening();
                ScaleStart();
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR AT STARTUP Call Dev on 1153 \n" +
                                ex.Message, "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

                Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                   Properties.Settings.Default.ErrorLogloc,
                                   DateTime.Now + " --> " +
                                   "ERROR AT STARTUP" +
                                   ex.Message +
                                   " " +
                                   ex.TargetSite);
            }

        }
        #endregion

        #region Report Information To DataGrid
        private void BinsScannedReport(DataGridView dataGridReport,
                                       DataTable scannedBins,
                                       string headings,
                                       Label lbl_Details)
        {
            lbl_Details.Text = (Properties.Settings.Default.ScanWorkStation + "  " + headings).ToUpper();
            dataGridReport.DataSource = scannedBins;
            dataGridReport.ClearSelection();
            int _BinTotal = 0, _massTotal = 0;
            {
                foreach (DataRow Row in scannedBins.Rows)
                {
                    _BinTotal += Convert.ToInt32(Row["FBins"]);
                    _massTotal += Convert.ToInt32(Row["Nett Actual (kg)"]);
                }
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.Font = new Font(dataGridReport.Font, FontStyle.Bold);
                dataGridReport.Rows[dataGridReport.Rows.Count - 1].DefaultCellStyle = style;
                dataGridReport.Rows[dataGridReport.Rows.Count - 1].Cells[0].Value = "Total";
                dataGridReport.Rows[dataGridReport.Rows.Count - 1].Cells[2].Value = _BinTotal.ToString();
                dataGridReport.Rows[dataGridReport.Rows.Count - 1].Cells[3].Value = _massTotal.ToString();
            }
        }

        #endregion

        #region Method for printing label

        public void PrintSAPLabel()
        {
            try
            {
                if (PingHost(Properties.Settings.Default.PrinterIPAddress.Trim()))
                {

                    if (PrintLabel())
                    {
                        //SetControlStates();

                        error_Label.ForeColor = Color.DarkGreen;
                        error_Label.Text = string.Format(_msg_BatchProcesssed,
                                                         lignoBinCaptures.SapBinId.ToString());

                        CallBinsScannedReportdetails();
                    }
                    else
                    {
                        error_Label.ForeColor = Color.Crimson;
                        error_Label.Text = string.Format(_error_Printing,
                                                         lignoBinCaptures.SapBinId.ToString());
                        Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                           Properties.Settings.Default.ErrorLogloc,
                                           DateTime.Now +
                                           " --> " +
                                           string.Format(_error_Printing,
                                                         lignoBinCaptures.SapBinId.ToString()));
                    }
                }
                else
                {
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = string.Format(_error_RegisterPrinter,
                                                     lignoBinCaptures.SapBinId.ToString());
                }
            }
            catch (Exception ex)
            {

                Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                   Properties.Settings.Default.ErrorLogloc,
                                    DateTime.Now + " --> "
                                    + string.Format(_error_TextFile,
                                                    lignoBinCaptures.SapBinId.ToString())
                                    + ex.Message
                                    + " "
                                    + ex.TargetSite);
            }
        }

        public Boolean PrintLabel()
        {
            try
            {
                String printerBrand = "";
                String printerIPAddress = "";
                String printForm = "";
                String rawLabelData = "";
                String transformedLabelData = "";

                printerBrand = lignoBinCaptures.GetPrinterBrand();

                printerIPAddress = Properties.Settings.Default.PrinterIPAddress.Trim();

                rawLabelData = lignoBinCaptures.GetLabelData();

                XmlDocument docDATA = new XmlDocument();

                docDATA.LoadXml(rawLabelData);

                XmlDocument docXSLT = new XmlDocument();

                if (printerBrand == "ZEBRA")
                {
                    docXSLT.Load(Properties.Settings.Default.ZebraXSLT.Trim().ToUpper());

                    printForm = Properties.Settings.Default.ZebraFORM.Trim().ToUpper();
                }
                else
                {
                    docXSLT.Load(Properties.Settings.Default.PrintronixXSLT.Trim().ToUpper());

                    printForm = Properties.Settings.Default.PrintronixFORM.Trim().ToUpper();
                }


                Int32 labelQuantity = Convert.ToInt32(docDATA.SelectSingleNode("/ROWSET/no_labels").InnerText);

                transformedLabelData = Transform(docDATA, docXSLT);


                if (transformedLabelData == "TRANSFORMATION_ERROR")
                {
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = string.Format(_error_Transform, lignoBinCaptures.SapBinId.ToString());
                }
                else
                {
                    if (CreatePrintTextFile(transformedLabelData))
                    {
                        if (PingHost(printerIPAddress))
                        {

                            /*********************************************************************************************
                            * Code below works but LPR is not recognized - Keep for rainy day
                            * 
                            * String printCommandForm1 = "\"" + "\\\\" +"TUGMC013IS\\c$\\Windows\\WinSxS\\amd64_microsoft - windows - p..ting - lprportmonitor_31bf3856ad364e35_10.0.16299.15_none_d6efcd73aef30c2f\\lpr.exe" + "\"" + " -S " + "168.155.214.201" + " -P any " + "\"" + Properties.Settings.Default.PrintTextFile.Trim().ToUpper() + "\"";
                            * String printCommandForm2 = "lpr.exe" + " -S " + "168.155.214.201" + " -P any " + "\"" + Properties.Settings.Default.PrintTextFile.Trim().ToUpper() + "\"";
                            * 
                            * ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/C " + @printCommandForm);
                            * procStartInfo.RedirectStandardOutput = true;
                            * procStartInfo.UseShellExecute = false;
                            * procStartInfo.RedirectStandardError = true;
                            * procStartInfo.CreateNoWindow = true;
                            *
                            * // start process
                            * Process proc = new Process();
                            * proc.StartInfo = procStartInfo;
                            * proc.Start();
                            *
                            * proc.WaitForExit();
                            *
                            * // read process output
                            * string cmdError = proc.StandardError.ReadToEnd();
                            * string cmdOutput = proc.StandardOutput.ReadToEnd();
                            * 
                            ********************************************************************************************/

                            Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                            clientSocket.NoDelay = true;

                            IPAddress ip = IPAddress.Parse(printerIPAddress);

                            IPEndPoint ipep = new IPEndPoint(ip, Properties.Settings.Default.PrinterPortNumber);

                            clientSocket.Connect(ipep);

                            byte[] filePrintFormBytes = File.ReadAllBytes(printForm);

                            byte[] filePrintDataBytes = File.ReadAllBytes(Properties.Settings.Default.PrintTextFile.Trim());

                            for (int i = 0; i < labelQuantity; i++)
                            {
                                clientSocket.Send(filePrintFormBytes);

                                clientSocket.Send(filePrintDataBytes);
                            }

                            clientSocket.Close();
                        }
                        else
                        {
                            error_Label.ForeColor = Color.Crimson;
                            error_Label.Text = string.Format(_error_Printing, lignoBinCaptures.SapBinId.ToString());

                        }
                    }
                    else
                    {
                        error_Label.ForeColor = Color.Crimson;
                        error_Label.Text = string.Format(_error_TextFile, lignoBinCaptures.SapBinId.ToString());
                        MessageBox.Show(string.Format(_error_TextFile, lignoBinCaptures.SapBinId.ToString()));
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                // MessageBox.Show("  " + ex.Message+ "      " + ex.TargetSite);
                // MessageBox.Show(string.Format(_error_TextFile, lignoBinCaptures.SapBinId.ToString()));
                Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath, Properties.Settings.Default.ErrorLogloc,
                                   DateTime.Now + " --> "
                                   + string.Format(_error_TextFile, lignoBinCaptures.SapBinId.ToString())
                                   + ex.Message
                                   + " "
                                   + ex.TargetSite);
                return false;
            }
        }

        public String Transform(XmlDocument doc, XmlDocument stylesheet)
        {
            try
            {
                XslCompiledTransform transform = new XslCompiledTransform();

                transform.Load(stylesheet);

                StringWriter writer = new StringWriter();

                XmlReader xmlReadB = new XmlTextReader(new StringReader(doc.DocumentElement.OuterXml));

                transform.Transform(xmlReadB, null, writer);

                return writer.ToString();
            }
            catch (Exception)
            {
                return "TRANSFORMATION_ERROR";
            }
        }


        public Boolean CreatePrintTextFile(String pLabelData)
        {
            try
            {
                File.WriteAllText(Properties.Settings.Default.PrintTextFile.Trim(), pLabelData);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private Boolean PingHost(string nameOrAddress)
        {
            Boolean pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress, Convert.ToInt32(Properties.Settings.Default.PrinterPingTimeOut));

                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }
        #endregion

        #region enables disables text boxes
        private void ScaleInputState(string scalecodestate)
        {
            if (lignoBinCaptures.ScaleActiveStates(scalecodestate) == true)
            {
                ScaleActiveStateEnabled(scalecodestate);
            }
            else
            {
                ScaleActiveStateDisabled(scalecodestate);

            }
        }

        private void ScaleActiveStateDisabled(string scalecodestate)
        {
            switch (scalecodestate)
            {
                case scaleCodeState:
                    txtUnloaded.ReadOnly = true;
                    txtLoaded.ReadOnly = true;
                    chkScalelState.Checked = false;
                    error_Label.ForeColor = Color.Green;
                    error_Label.Text = "SCALE AUTOMATIC MODE ACTIVATED";
                    break;

                case scaleCodeMRO:

                    chkMassRound.Checked = false;
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = "ROUNDING MODE DISABLED";
                    break;

                case scaleCodeLtr:
                    lblLoadedLtr.Visible = false;
                    txtLiters.Visible = false;
                    chkScaleLtr.Checked = false;
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = "LITRES CAPTURE DISABLED";
                    CheckLiters = false;
                    break;
                case scaleNetMasscalcCode:
                    chkEnableNetMassCalc.Checked = false;
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = "NET MASS CALC DISABLED";
                    break;

                case specialTestCode:
                     chkSpecialTest.Checked = false;
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = "SPECIAL DISABLED";
                    lbltank.ForeColor = Color.Black;
                    lbltank.Text = "Tank Selection";
                    break;

                case ManualTestCapture:
                    chkManualCapture.Checked = false;
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = "Manual Capture DISABLED";
                    break;
                //case manualTestInputCode:
                //    chkManualTestInput.Checked = false;
                //    error_Label.ForeColor = Color.Crimson;
                //    error_Label.Text = "MANUAL TEST INPUT DISABLED";

                //    break;


                default:
                    break;
            }
        }

        private void ScaleActiveStateEnabled(string scalecodestate)
        {
            switch (scalecodestate)
            {
                case scaleCodeState:

                    txtUnloaded.ReadOnly = false;
                    txtLoaded.ReadOnly = false;
                    chkScalelState.Checked = true;
                    error_Label.ForeColor = Color.Green;

                    error_Label.Text = "SCALE MANUAL MODE ACTIVATED";
                    break;

                case scaleCodeMRO:
                    chkMassRound.Checked = true;
                    error_Label.ForeColor = Color.Green;
                    error_Label.Text = "ROUNDING MODE ACTIVATED";
                    break;

                case scaleCodeLtr:
                    lblLoadedLtr.Visible = true;
                    txtLiters.Visible = true;
                    txtLiters.Text = "";
                    chkScaleLtr.Checked = true;
                    error_Label.ForeColor = Color.Green;
                    error_Label.Text = "LITRES CAPTURE ACTIVATED";
                    CheckLiters = true;
                    break;
                case scaleNetMasscalcCode:
                    chkEnableNetMassCalc.Checked = true;
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = "NET MASS CALC ACTIVATED";
                    break;
                case specialTestCode:
                    chkSpecialTest.Checked = true;
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = "SPECIAL ACTIVATED";
                    lbltank.ForeColor = Color.Crimson;
                    lbltank.Text = "Tank Selection: Special Test ACTIVATED";
                    break;

                case ManualTestCapture:
                    chkManualCapture.Checked = true;
                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = "Manual Capture ACTIVATED";
                    break;
                //case manualTestInputCode:
                //    chkManualTestInput.Checked = true;
                //    error_Label.ForeColor = Color.Crimson;
                //    error_Label.Text = "MANUAL Test INPUT ACTIVATED";
                //    break;
                default:
                    break;
            }
        }



        #endregion

        #region Hyperlink to error log and display with CmTrace
        private void lnkLblOpenErrorLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(logviewer, errorlogfile);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Unable to open Error Log \n" + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        #endregion

        #region Scale Weight Button to get Manual or automatic scale values
        private void bntScale_Click(object sender, EventArgs e)
        {
            try
            {


                PopulateScaleRanges();

                if (CheckLiters == true)
                {
                    SetControlStatesWhenRegisteringFlowbin(false);
                    LitersInputEnabled();
                }
                else
                {
                    SetControlStatesWhenRegisteringFlowbin(false);
                    LitersInputDisabled();

                }
            }
            catch (Exception ex)
            {


                MessageBox.Show("ERROR AT SCALE Call Dev on 1153 \n" +
                                ex.Message, "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

                Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                   Properties.Settings.Default.ErrorLogloc,
                                   DateTime.Now + " --> " +
                                   "ERROR AT SCALE" +
                                   ex.Message +
                                   " " +
                                   ex.TargetSite);
            }
        }

        private void LitersInputEnabled()
        {
            switch (ScaleWeighOrder)
            {
                case 0:
                    UnloadedScaleScheck();
                    break;
                case 1:
                    LoadedScaleCheck();
                    break;
                case 2:
                    if (LitersLoadedScaleCheck() == true)
                    {
                        bntScale.Enabled = false;
                        cmbTanks.Enabled = true; // enable tanks combo box for selection
                    }
                    break;
                default:
                    break;
            }
        }

        private void LitersInputDisabled()
        {
            switch (ScaleWeighOrder)
            {
                case 0:
                    UnloadedScaleScheck();
                    break;
                case 1:

                    if (LoadedScaleCheck() == true)
                    {
                        txtLiters.Text = "0";
                        bntScale.Enabled = false;
                        cmbTanks.Enabled = true;
                    }

                    break;

                default:
                    break;
            }
        }

        private Boolean UnloadedScaleScheck()
        {
            Boolean check = false;
            if (chkScalelState.Checked == true)
            { }
            else
                txtUnloaded.Text = lblScale.Text;


            if (!Int32.TryParse(txtUnloaded.Text, out int j))
            {
                error_Label.ForeColor = Color.Crimson;
                error_Label.Text = "Unloaded Weight\n" + _error_Weight;
                check = false;
            }
            else
            {
                if (lignoBinCaptures.ValidateScaleInputs(Convert.ToInt32(txtUnloaded.Text), scaleCodeUnloadedCheck) == true)
                {
                    error_Label.ForeColor = Color.Green;
                    error_Label.Text = "Unloaded Weight" + _Weight_Capture_sucessfull;
                    lblFlowBinNumber.Text = lignoBinCaptures.GetFlowBinNumber();
                    txtLoaded.Focus();
                    txtUnloaded.Enabled = false;
                    bntScale.Text = "Loaded Weight";
                    check = true;
                    ScaleWeighOrder = 1;
                }
                else
                {
                    error_Label.ForeColor = Color.Crimson; ;
                    error_Label.Text = "Unloaded Weight NOT IN RANGE :" + RangeUnloadedMin + "KG"
                                                                        + " - "
                                                                        + RangeUnloadedMax + "KG";
                    check = false;
                }
            }
            return check;
        }
        private Boolean LoadedScaleCheck()
        {
            Boolean check = false;


            if (chkScalelState.Checked == true)
            { }
            else
                txtLoaded.Text = lblScale.Text;

            if (!Int32.TryParse(txtLoaded.Text, out int j))
            {
                error_Label.ForeColor = Color.Crimson;
                error_Label.Text = "Loaded Weight\n" + _error_Weight;

                check = false;
            }
            else
            {
                if (lignoBinCaptures.ValidateScaleInputs(Convert.ToInt32(txtLoaded.Text), scaleCodeLoadedCheck) == true)
                {
                    error_Label.ForeColor = Color.Green;
                    error_Label.Text = "loaded Weight" + _Weight_Capture_sucessfull;
                    txtLiters.Focus();
                    txtLoaded.Enabled = false;
                    if (chkScaleLtr.Checked == true)

                        bntScale.Text = "Number of Litres";
                    else
                    {
                        bntScale.Text = "loaded Weight";
                        ScaleWeighOrder = 2;
                    }

                    if (chkEnableNetMassCalc.Checked == true)
                    {
                        lblNettMass.Text = (Convert.ToInt32(txtLoaded.Text) - Convert.ToInt32(txtUnloaded.Text)).ToString();
                    }
                    else
                        lblNettMass.Text = txtLoaded.Text;

                    check = true;
                    ScaleWeighOrder = 2;
                }
                else
                {
                    error_Label.ForeColor = Color.Crimson; ;
                    error_Label.Text = "Loaded Weight NOT IN RANGE :" + RangeLoadedMin + "KG"
                                                                        + " - "
                                                                        + RangeLoadedMax + "KG";
                    check = false;
                }

            }
            return check;
        }
        private Boolean LitersLoadedScaleCheck()
        {
            Boolean check = false;

            if (!Decimal.TryParse(txtLiters.Text, out Decimal j))
            {
                error_Label.ForeColor = Color.Crimson;
                error_Label.Text = "Loaded Litres\n" + _error_Weight;

                check = false;
            }
            else
            {
                if (lignoBinCaptures.ValidateScaleInputs(Convert.ToInt32(Math.Round(Convert.ToDecimal(txtLiters.Text), 1)), scaleCodeLtr) == true)
                {
                    txtLiters.Text = Convert.ToDecimal(Math.Round(Convert.ToDecimal(txtLiters.Text), 1)).ToString();
                    error_Label.ForeColor = Color.Green;
                    error_Label.Text = "loaded Litres\n" + "Captured";
                    txtLiters.Enabled = false;
                    bntScale.Text = "Weight And Litres Captured ";
                    check = true;
                    ScaleWeighOrder = 3;
                }
                else
                {
                    error_Label.ForeColor = Color.Crimson; ;
                    error_Label.Text = "Loaded Litres NOT IN RANGE:" + RangeLtrMin + "L"
                                                                        + " - "
                                                                        + RangeLtrMax + "L";
                    check = false;
                }

            }
            return check;
        }

        #endregion

        #region Sets control states when Registering FlowBins
        private void SetControlStatesWhenRegisteringFlowbin(Boolean state)
        {
            chkMassRound.Enabled = state;
            chkScalelState.Enabled = state;
            chkScaleLtr.Enabled = state;
            chkEnableNetMassCalc.Enabled = state;
            chkSpecialTest.Enabled = state;
            chkManualCapture.Enabled = state;
            bntReprint.Enabled = state;
            bntScaleSettings.Enabled = state;
        }
        #endregion



    
        #region Setting Scale input Method Auto or manual

        //private void chkManualTestInput_MouseClick(object sender, MouseEventArgs e)
        //{
        //    GetAuthDetails();

        //    if (AuthSuccess == true)
        //    {
        //        lignoBinCaptures.UpdateScaleActiveStates(manualTestInputCode);
        //        ScaleInputState(scaleNetMasscalcCode);
        //        lignoBinCaptures.Authlog(manualTestInputCode, EmpNo, Username, Reason);
        //    }

        //    ManualTestTxtBoxState();
        //    ScaleInputState(manualTestInputCode);
        //    CleanAuthBoxDetails();
        //}

        //private void  ManualTestTxtBoxState()
        //{
        //    if (chkManualTestInput.Checked==true)
        // txtPh.ReadOnly= txtSolids.ReadOnly= txtInsolubles.ReadOnly= txtDensity.ReadOnly = false;
            
        //    else
        //        txtPh.ReadOnly = txtSolids.ReadOnly = txtInsolubles.ReadOnly = txtDensity.ReadOnly = true;
        //}

        private void chkScalelState_MouseClick(object sender, MouseEventArgs e)
        {
            GetAuthDetails();

            if (AuthSuccess == true)
            {
                lignoBinCaptures.UpdateScaleActiveStates(scaleCodeState);
                ScaleInputState(scaleCodeState);
                lignoBinCaptures.Authlog(scaleCodeState, EmpNo, Username, Reason);
            }

            ScaleInputState(scaleCodeState);
            CleanAuthBoxDetails();
        }

        private void chkEnableNetMassCalc_MouseClick_1(object sender, MouseEventArgs e)
        {
            GetAuthDetails();

            if (AuthSuccess == true)
            {
                lignoBinCaptures.UpdateScaleActiveStates(scaleNetMasscalcCode);
                ScaleInputState(scaleNetMasscalcCode);
                lignoBinCaptures.Authlog(scaleNetMasscalcCode, EmpNo, Username, Reason);
            }

            ScaleInputState(scaleNetMasscalcCode);
            CleanAuthBoxDetails();
        }
        #endregion

        #region Clear Test Results Display Boxes
        void ClearTxtboxTests(string val,bool test )
        {
            if (test == true)
            {
                txtDensity.BackColor = Color.White;
                txtInsolubles.BackColor = Color.White;
                txtSolids.BackColor = Color.White;
                txtPh.BackColor = Color.White;

                txtPh.Text = val;
                txtSolids.Text = val;
                txtDensity.Text = val;
                txtInsolubles.Text = val;
            }
           
            txtPhMin.Text = val;
            txtPhMax.Text = val;
            txtPhTarget.Text = val;
            txtPhLrl.Text = val;
            txtPhUrl.Text = val;
            
            
            txtSolidsMin.Text = val;
            txtSolidsMax.Text = val;
            txtSolidsTarget.Text = val;
            txtSolidsLrl.Text = val;
            txtSolidsUrl.Text = val;
            
            
            txtInsolublesMin.Text = val;
            txtInsolublesMax.Text = val;
            txtInsolublesTarget.Text = val;
            txtInsolublesLrl.Text = val;
            txtInsolublesUrl.Text = val;
            
            
            txtDensityMin.Text = val;
            txtDensityMax.Text = val;
            txtDensityTarget.Text = val;
            txtDensityLrl.Text = val;
            txtDensityUrl.Text = val;

        }
        #endregion

        #region Get TEST Values for LIS information Section of application
        private void GetPhValues()
        {
            var LIS_TEST_NO = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, 1).Rows[0]["LIS_TEST_NO"].ToString());
            var SAMPLEPOINT_ID = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, 1).Rows[0]["SAMPLEPOINT_ID"].ToString());
            var LIS_DESCRIPTION = lignoBinCaptures.GetTestInformation(cmbTanks.Text, 1).Rows[0]["LIS_DESCRIPTION"].ToString();

            txtPh.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["AvgResults"].ToString();
            txtPhMin.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["SpecMin"].ToString();
            txtPhMax.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["SpecMax"].ToString();
            txtPhTarget.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["Aim"].ToString();
            txtPhLrl.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["avgLRL"].ToString();
            txtPhUrl.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["avgURL"].ToString();
        }

        private void GetSolidsValues()
        {
            var LIS_TEST_NO = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, 2).Rows[0]["LIS_TEST_NO"].ToString());
            var SAMPLEPOINT_ID = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, 2).Rows[0]["SAMPLEPOINT_ID"].ToString());
            var LIS_DESCRIPTION = lignoBinCaptures.GetTestInformation(cmbTanks.Text, 2).Rows[0]["LIS_DESCRIPTION"].ToString();

            txtSolids.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["AvgResults"].ToString();
            txtSolidsMin.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["SpecMin"].ToString();
            txtSolidsMax.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["SpecMax"].ToString();
            txtSolidsTarget.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["Aim"].ToString();
            txtSolidsLrl.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["avgLRL"].ToString();
            txtSolidsUrl.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["avgURL"].ToString();
        }
        private void GetInsolublesValues()
        {
            var LIS_TEST_NO = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, 3).Rows[0]["LIS_TEST_NO"].ToString());
            var SAMPLEPOINT_ID = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, 3).Rows[0]["SAMPLEPOINT_ID"].ToString());
            var LIS_DESCRIPTION = lignoBinCaptures.GetTestInformation(cmbTanks.Text, 3).Rows[0]["LIS_DESCRIPTION"].ToString();

            txtInsolubles.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["AvgResults"].ToString();
            txtInsolublesMin.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["SpecMin"].ToString();
            txtInsolublesMax.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["SpecMax"].ToString();
            txtInsolublesTarget.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["Aim"].ToString();
            txtInsolublesLrl.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["avgLRL"].ToString();
            txtInsolublesUrl.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["avgURL"].ToString();
        }

        private void GetDensityValues()
        {
            var LIS_TEST_NO = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, 4).Rows[0]["LIS_TEST_NO"].ToString());
            var SAMPLEPOINT_ID = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, 4).Rows[0]["SAMPLEPOINT_ID"].ToString());
            var LIS_DESCRIPTION = lignoBinCaptures.GetTestInformation(cmbTanks.Text, 4).Rows[0]["LIS_DESCRIPTION"].ToString();

            txtDensity.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["AvgResults"].ToString();
            txtDensityMin.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["SpecMin"].ToString();
            txtDensityMax.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["SpecMax"].ToString();
            txtDensityTarget.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["Aim"].ToString();
            txtDensityLrl.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["avgLRL"].ToString();
            txtDensityUrl.Text = lignoBinCaptures.LignoLIS_Data(LIS_TEST_NO, SAMPLEPOINT_ID, LIS_DESCRIPTION).Rows[0]["avgURL"].ToString();
        }
        #endregion


        private void chkSpecialTest_MouseClick(object sender, MouseEventArgs e)
        {
            GetAuthDetails();

            if (AuthSuccess == true)
            {
                lignoBinCaptures.UpdateScaleActiveStates(specialTestCode);
                ScaleInputState(specialTestCode);
                lignoBinCaptures.Authlog(specialTestCode, EmpNo, Username, Reason);
               
            }
            ClearTxtboxTests("",true);
            PopulateTanksComboBox();
            ScaleInputState(specialTestCode);
            CleanAuthBoxDetails();
        }

        private void btnCHKManualTests_Click(object sender, EventArgs e)
        {
            if (checkNumericValueInputs()==true)
            {
                if (lignoBinCaptures.AuthBoxStateTank)
                    CallAuthForOutofSpecTests();
                else
                    cmbProduct.Enabled = true;

                if (!CheckLignoLiquidSpecs())
                {
                    cmbProduct.Enabled = true;
                }
               
                    lisdataloaded = true;
                //btnCHKManualTests.Enabled = false;
            }
        }

       private Boolean checkNumericValueInputs()
        {
            bool ph = false; bool d = false; bool s = false; bool ins = false;
            bool check = false;
            decimal parsedValue;
            if (!decimal.TryParse(txtDensity.Text, out parsedValue))
            {

                MessageBox.Show("Density test Value is not a number");
            }
            else
                d = true;

            if (!decimal.TryParse(txtPh.Text, out parsedValue))
            {
                MessageBox.Show("PH test Value is not a number");
            }
            else
                ph = true;

            if (!decimal.TryParse(txtSolids.Text, out parsedValue))
            {
                MessageBox.Show("Solids test Value is not a number");
            }
            else
                s = true;

            if (!decimal.TryParse(txtInsolubles.Text, out parsedValue))
            {
                MessageBox.Show("Insolubles testValue is not a number");
            }
            else
                ins = true;

            if (ph == true & d == true & s == true & ins == true)
                check = true;
            else
                check = false;

            return check;
        }


        #region Tanks Combo box populating test results and getting out of spec values
        private void cmbTanks_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableForManualTestCapture();
            // ManualTestTxtBoxState();
            if (chkManualCapture.Checked == true)
            {
                btnCHKManualTests.Visible = true;
                btnCHKManualTests.Enabled = true;
                ClearTxtboxTests("", true);
                EnableForManualTestCapture();
              
            }
 
            else
            {
                btnCHKManualTests.Visible = false;
                btnCHKManualTests.Enabled = false;

                DisableForAutoTestCapture();
                AutoCheckTankTests();
            }
        }

        private void AutoCheckTankTests()
        {
            if (cmbTanksLoaded == true)
            {
                ClearTxtboxTests("",true);
                error_Label.Text = "";

                try

                {
                    ClearTxtboxTests("",true);
                    GetTestsAndPopulateLisInfoSection();
                    if (lignoBinCaptures.AuthBoxStateTank)
                        CallAuthForOutofSpecTests();
                    else
                        cmbProduct.Enabled = true;

                    if (!CheckLignoLiquidSpecs())
                        cmbProduct.Enabled = true;

                    lisdataloaded = true;
                }
                catch (Exception ex)
                {
                    StringBuilder tests = new StringBuilder();
                    #region Error Message Capture
                    DisableControls_When_Lis_Load_Fails();
                    // MessageBox.Show("Unable to get Values From LIS " + ex.Message);
                    if (txtPh.Text == "")
                        tests.Append("PH,");
                    if (txtSolids.Text == "")
                        tests.Append("Solids,");
                    if (txtInsolubles.Text == "")
                        tests.Append("Insolubles,");
                    if (txtDensity.Text == "")
                        tests.Append("Density,");

                    error_Label.ForeColor = Color.Crimson;
                    error_Label.Text = ("LIS Test WAS NOT FOUND FOR " +
                                        cmbTanks.Text +
                                        "\n\n" +
                                        "Missing Test :" +
                                        tests.ToString() +
                                        "\n\nCHECK WITH MAIN LAB").ToUpper();

                    MessageBox.Show(("LIS Test WAS NOT FOUND FOR " +
                                     cmbTanks.Text.ToUpper() +
                                     " \n\n" +
                                     "Missing Test :" +
                                     tests.ToString() +
                                     "\n\nCHECK WITH MAIN LAB").ToUpper(),

                                   "Missing LIS TESTS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                       Properties.Settings.Default.ErrorLogloc,
                                       DateTime.Now +
                                       " --> " + "" +
                                       "ERROR" +
                                       "LIS Test WAS NOT FOUND FOR " +
                                       cmbTanks.Text.ToUpper() +
                                       "\n\n" +
                                       "Missing Test :" +
                                       tests.ToString().ToUpper() +
                                       "\n\nCHECK WITH MAIN LAB" +
                                       ex.Message +
                                       " " +
                                       ex.TargetSite);
                    #endregion
                }
            }
        }
        #endregion

        //Manual test removed due to special test
        //private void btnCheckTest_Click(object sender, EventArgs e)
        //{
        //    ManualTestTxtBoxState();
        //    ClearTxtboxTests("0", false);
        //    if (!CheckLignoLiquidSpecs())
        //        cmbProduct.Enabled = true;

        //    if (lignoBinCaptures.AuthBoxStateTank)
        //        CallAuthForOutofSpecTests();
        //    else
        //        cmbProduct.Enabled = true;

        //    lisdataloaded = true;

        //}
        #region Populate with LIS information
        private void GetTestsAndPopulateLisInfoSection()
        {
            GetPhValues();
            GetSolidsValues();
            GetInsolublesValues();
            GetDensityValues();
        }
        #endregion

        #region Call auth Box for out of spec tests Get FM to approve
        private void CallAuthForOutofSpecTests()
        {
            AuthSuccess = false;
            ResetOnSelection();
            if (CheckLignoLiquidSpecs())
            {
                if (MessageBox.Show(_msg_OutOfSpec, "Test Specifications NOT IN RANGE",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DisableControls_When_Lis_Load_Fails();
                    GetAuthDetails();
                    if (AuthSuccess)
                    {
                        cmbProduct.Enabled = true;
                    }
                }
                else
                {
                    //     ResetOnSelection();
                }
            }
        }
        private void CallAuthForProductOutOfSpecTests()
        {
            AuthSuccess = false;
            // ResetOnSelection();
            bnt_ProcessBin.Enabled = false;
            if (CheckLignoProductSpecs())
            {
                if (MessageBox.Show(_msg_OutOfSpec, "Test Specifications NOT IN RANGE",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    // DisableControls_When_Lis_Load_Fails();
                    GetAuthDetails();
                    if (AuthSuccess)
                    {
                        bnt_ProcessBin.Enabled = true;
                    }
                }
                else
                {
                    bnt_ProcessBin.Enabled = false;
                }
            }
        }

        private void ResetOnSelection()
        {
            PopulateProductComboBox();
            DisableControls_When_Lis_Load_Fails();
            cmbProduct.Enabled = false;
        }
        #endregion

        //This will be used if a test result is missing from LIS
        #region Disable controls When LIS Load Fails 
        private void DisableControls_When_Lis_Load_Fails()
        {
            lisdataloaded = false;
            bnt_ProcessBin.Enabled = false;
            cmbProduct.Enabled = false;
        }

        #endregion

        #region Resets All controls back to start up
        private void Reset_Everything()
        {
            //Check Scale state and if manual re-enables text boxes
            if (lignoBinCaptures.ScaleActiveStates(scaleCodeState))
            {
                txtUnloaded.Enabled = true;
                txtLoaded.Enabled = true;
                txtLiters.Enabled = true;
            }

            txtLiters.Enabled = true;

            ScaleWeighOrder = 0;
            txtUnloaded.Text = "";
            txtLoaded.Text = "";
            txtLiters.Text = "";
            lblNettMass.Text = "";
            lblFlowBinNumber.Text = "";
            PopulateTanksComboBox();
            PopulateProductComboBox();
            cmbTanks.Enabled = false;
            cmbProduct.Enabled = false;
            ClearTxtboxTests("",true);
            bnt_ProcessBin.Enabled = false;
            bntScale.Enabled = true;
            bntScale.Text = "Unloaded Weight";
            CallBinsScannedReportdetails();

            //Clear out Auth details
            Username = "";
            EmpNo = "";
            Reason = "";
            lignoBinCaptures.Username = "";
            lignoBinCaptures.Password = "";
            lignoBinCaptures.EmpNo = "";

            //used to enable controls after registration process
            SetControlStatesWhenRegisteringFlowbin(true);

            btnCHKManualTests.Visible = false;

        }
        #endregion

        #region Concat Test results for TBL_TEST_RESULTS_FLOWBINS Table to have a record of results for tracing back
        private void ConcatResultsToSingleString(int Testno)
        {

            var LIS_TEST_NO = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, Testno)
                                              .Rows[0]["LIS_TEST_NO"].ToString());
            var SAMPLEPOINT_ID = Convert.ToInt32(lignoBinCaptures.GetTestInformation(cmbTanks.Text, Testno)
                                              .Rows[0]["SAMPLEPOINT_ID"].ToString());
            var LIS_DESCRIPTION = lignoBinCaptures.GetTestInformation(cmbTanks.Text, Testno)
                                              .Rows[0]["LIS_DESCRIPTION"].ToString();

            switch (Testno)
            {
                case 1:
                    foreach (DataRow dtRow in lignoBinCaptures.LignoLIS_DataList(LIS_TEST_NO,
                                                                                 SAMPLEPOINT_ID,
                                                                                 LIS_DESCRIPTION).Rows)
                    {

                        ResultsID_PH = string.Concat(ResultsID_PH) +
                                       string.Concat(dtRow["ResultId"].ToString()) + ",";
                        DateFrom_PH = string.Concat(DateFrom_PH) +
                                      string.Concat(dtRow["Date_From"].ToString()) + ",";
                        DateTo_PH = string.Concat(DateTo_PH) +
                                    string.Concat(dtRow["Date_to"].ToString()) + ",";
                        ResultsTimeStamp_PH = string.Concat(ResultsTimeStamp_PH) +
                                              string.Concat(dtRow["ResultTimeStamp"].ToString() + ",");
                        TestValueResults_List_PH = string.Concat(TestValueResults_List_PH) + "spMx:" +
                                                   string.Concat(dtRow["SpecMax"].ToString()) + "," + "spMi:" +
                                                   string.Concat(dtRow["SpecMin"].ToString()) + "," + "Rs:" +
                                                   string.Concat(dtRow["Results"].ToString()) + "," + "Tar:" +
                                                   string.Concat(dtRow["Aim"].ToString()) + "," + "Lrl:" +
                                                   string.Concat(dtRow["LRL"].ToString()) + "," + "Url:" +
                                                   string.Concat(dtRow["URL"].ToString()) + ",";
                    }
                    break;
                case 2:
                    foreach (DataRow dtRow in lignoBinCaptures.LignoLIS_DataList(LIS_TEST_NO,
                                                                                 SAMPLEPOINT_ID,
                                                                                 LIS_DESCRIPTION).Rows)
                    {

                        ResultsID_Solids = string.Concat(ResultsID_Solids) +
                                           string.Concat(dtRow["ResultId"].ToString()) + ",";
                        DateFrom_Solids = string.Concat(DateFrom_Solids) +
                                          string.Concat(dtRow["Date_From"].ToString()) + ",";
                        DateTo_Solids = string.Concat(DateTo_Solids) +
                                        string.Concat(dtRow["Date_to"].ToString()) + ",";
                        ResultsTimeStamp_Solids = string.Concat(ResultsTimeStamp_Solids) +
                                                  string.Concat(dtRow["ResultTimeStamp"].ToString() + ",");
                        TestValueResults_List_Solids = string.Concat(TestValueResults_List_Solids) + "spMx:" +
                                                       string.Concat(dtRow["SpecMax"].ToString()) + "," + "spMi:" +
                                                       string.Concat(dtRow["SpecMin"].ToString()) + "," + "Rs:" +
                                                       string.Concat(dtRow["Results"].ToString()) + "," + "Tar:" +
                                                       string.Concat(dtRow["Aim"].ToString()) + "," + "Lrl:" +
                                                       string.Concat(dtRow["LRL"].ToString()) + "," + "Url:" +
                                                       string.Concat(dtRow["URL"].ToString()) + ",";

                    }

                    break;
                case 3:
                    foreach (DataRow dtRow in lignoBinCaptures.LignoLIS_DataList(LIS_TEST_NO,
                                                                                 SAMPLEPOINT_ID,
                                                                                 LIS_DESCRIPTION).Rows)
                    {

                        ResultsID_Insolubles = string.Concat(ResultsID_Insolubles) +
                                               string.Concat(dtRow["ResultId"].ToString()) + ",";
                        DateFrom_Insolubles = string.Concat(DateFrom_Insolubles) +
                                              string.Concat(dtRow["Date_From"].ToString()) + ",";
                        DateTo_Insolubles = string.Concat(DateTo_Insolubles) +
                                            string.Concat(dtRow["Date_to"].ToString()) + ",";
                        ResultsTimeStamp_Insolubles = string.Concat(ResultsTimeStamp_Insolubles) +
                                                      string.Concat(dtRow["ResultTimeStamp"].ToString() + ",");
                        TestValueResults_List_Insolubles = string.Concat(TestValueResults_List_Insolubles) + "spMx:" +
                                                           string.Concat(dtRow["SpecMax"].ToString()) + "," + "spMi:" +
                                                           string.Concat(dtRow["SpecMin"].ToString()) + "," + "Rs:" +
                                                           string.Concat(dtRow["Results"].ToString()) + "," + "Tar:" +
                                                           string.Concat(dtRow["Aim"].ToString()) + "," + "Lrl:" +
                                                           string.Concat(dtRow["LRL"].ToString()) + "," + "Url:" +
                                                           string.Concat(dtRow["URL"].ToString()) + ",";
                    }

                    break;

                case 4:
                    foreach (DataRow dtRow in lignoBinCaptures.LignoLIS_DataList(LIS_TEST_NO,
                                                                                 SAMPLEPOINT_ID,
                                                                                 LIS_DESCRIPTION).Rows)
                    {

                        ResultsID_Density = string.Concat(ResultsID_Density) +
                                            string.Concat(dtRow["ResultId"].ToString()) + ",";
                        DateFrom_Density = string.Concat(DateFrom_Density) +
                                           string.Concat(dtRow["Date_From"].ToString()) + ",";
                        DateTo_Density = string.Concat(DateTo_Density) +
                                         string.Concat(dtRow["Date_to"].ToString()) + ",";
                        ResultsTimeStamp_Density = string.Concat(ResultsTimeStamp_Density) +
                                                   string.Concat(dtRow["ResultTimeStamp"].ToString() + ",");
                        TestValueResults_List_Density = string.Concat(TestValueResults_List_Density) + "spMx:" +
                                                                 string.Concat(dtRow["SpecMax"].ToString()) + "," + "spMi:" +
                                                                 string.Concat(dtRow["SpecMin"].ToString()) + "," + "Rs:" +
                                                                 string.Concat(dtRow["Results"].ToString()) + "," + "Tar:" +
                                                                 string.Concat(dtRow["Aim"].ToString()) + "," + "Lrl:" +
                                                                 string.Concat(dtRow["LRL"].ToString()) + "," + "Url:" +
                                                                 string.Concat(dtRow["URL"].ToString()) + ",";
                    }

                    break;

                default:

                    break;
            }

        }
        #endregion

        #region Register FlowBin Ligno Liquid Test from LIS to PTS

        private void RegisterFlowBinTestResults(string FlowBinNumber, int Testno, string Samplepoint)
        {
            if (chkManualCapture.Checked == false)
            {

                ConcatResultsToSingleString(Testno);

                switch (Testno)
                {
                    case 1:
                        lignoBinCaptures.RegisterFlowBinTestResults(lblFlowBinNumber.Text,
                                                                    ResultsID_PH,
                                                                    ResultsTimeStamp_PH,
                                                                    DateFrom_PH,
                                                                    DateTo_PH,
                                                                    Testno,
                                                                    Convert.ToDecimal(txtPh.Text),
                                                                    Convert.ToDecimal(txtPhMin.Text),
                                                                    Convert.ToDecimal(txtPhMax.Text),
                                                                    Convert.ToDecimal(txtPhTarget.Text),
                                                                    Convert.ToDecimal(txtPhLrl.Text),
                                                                    Convert.ToDecimal(txtPhUrl.Text),
                                                                    TestValueResults_List_PH,
                                                                    Samplepoint);
                        break;

                    case 2:
                        lignoBinCaptures.RegisterFlowBinTestResults(lblFlowBinNumber.Text,
                                                                    ResultsID_Solids,
                                                                    ResultsTimeStamp_Solids,
                                                                    DateFrom_Solids,
                                                                    DateTo_Solids,
                                                                    Testno,
                                                                    Convert.ToDecimal(txtSolids.Text),
                                                                    Convert.ToDecimal(txtSolidsMin.Text),
                                                                    Convert.ToDecimal(txtSolidsMax.Text),
                                                                    Convert.ToDecimal(txtSolidsTarget.Text),
                                                                    Convert.ToDecimal(txtSolidsLrl.Text),
                                                                    Convert.ToDecimal(txtSolidsUrl.Text),
                                                                    TestValueResults_List_Solids,
                                                                    Samplepoint);
                        break;
                    case 3:
                        lignoBinCaptures.RegisterFlowBinTestResults(lblFlowBinNumber.Text,
                                                                    ResultsID_Insolubles,
                                                                    ResultsTimeStamp_Insolubles,
                                                                    DateFrom_Insolubles,
                                                                    DateTo_Insolubles,
                                                                    Testno,
                                                                    Convert.ToDecimal(txtInsolubles.Text),
                                                                    Convert.ToDecimal(txtInsolublesMin.Text),
                                                                    Convert.ToDecimal(txtInsolublesMax.Text),
                                                                    Convert.ToDecimal(txtInsolublesTarget.Text),
                                                                    Convert.ToDecimal(txtInsolublesLrl.Text),
                                                                    Convert.ToDecimal(txtInsolublesUrl.Text),
                                                                    TestValueResults_List_Insolubles,
                                                                    Samplepoint);
                        break;

                    case 4:
                        lignoBinCaptures.RegisterFlowBinTestResults(lblFlowBinNumber.Text,
                                                                    ResultsID_Density,
                                                                    ResultsTimeStamp_Density,
                                                                    DateFrom_Density,
                                                                    DateTo_Density,
                                                                    Testno,
                                                                    Convert.ToDecimal(txtDensity.Text),
                                                                    Convert.ToDecimal(txtDensityMin.Text),
                                                                    Convert.ToDecimal(txtDensityMax.Text),
                                                                    Convert.ToDecimal(txtDensityTarget.Text),
                                                                    Convert.ToDecimal(txtDensityLrl.Text),
                                                                    Convert.ToDecimal(txtDensityUrl.Text),
                                                                    TestValueResults_List_Density,
                                                                    Samplepoint);
                        break;

                    default:
                        break;
                }
            }
            else
            { 
                switch (Testno)
                {
                    case 1:
                        lignoBinCaptures.RegisterFlowBinTestResults(lblFlowBinNumber.Text,
                                                                    "0",
                                                                    DateTime.Now.ToString(),
                                                                    DateTime.Now.ToString(),
                                                                    DateTime.Now.ToString(),
                                                                    Testno,
                                                                    Convert.ToDecimal(txtPh.Text),
                                                                    Convert.ToDecimal(txtPhMin.Text),
                                                                    Convert.ToDecimal(txtPhMax.Text),
                                                                    Convert.ToDecimal(txtPhTarget.Text),
                                                                    Convert.ToDecimal(txtPhLrl.Text),
                                                                    Convert.ToDecimal(txtPhUrl.Text),
                                                                    "0",
                                                                    Samplepoint);
                        break;

                    case 2:
                        lignoBinCaptures.RegisterFlowBinTestResults(lblFlowBinNumber.Text,
                                                                    "0",
                                                                    DateTime.Now.ToString(),
                                                                    DateTime.Now.ToString(),
                                                                    DateTime.Now.ToString(),
                                                                    Testno,
                                                                    Convert.ToDecimal(txtSolids.Text),
                                                                    Convert.ToDecimal(txtSolidsMin.Text),
                                                                    Convert.ToDecimal(txtSolidsMax.Text),
                                                                    Convert.ToDecimal(txtSolidsTarget.Text),
                                                                    Convert.ToDecimal(txtSolidsLrl.Text),
                                                                    Convert.ToDecimal(txtSolidsUrl.Text),
                                                                    "0",
                                                                    Samplepoint);
                        break;
                    case 3:
                        lignoBinCaptures.RegisterFlowBinTestResults(lblFlowBinNumber.Text,
                                                                    "0",
                                                                    DateTime.Now.ToString(),
                                                                    DateTime.Now.ToString(),
                                                                    DateTime.Now.ToString(),
                                                                    Testno,
                                                                    Convert.ToDecimal(txtInsolubles.Text),
                                                                    Convert.ToDecimal(txtInsolublesMin.Text),
                                                                    Convert.ToDecimal(txtInsolublesMax.Text),
                                                                    Convert.ToDecimal(txtInsolublesTarget.Text),
                                                                    Convert.ToDecimal(txtInsolublesLrl.Text),
                                                                    Convert.ToDecimal(txtInsolublesUrl.Text),
                                                                    "0",
                                                                    Samplepoint);
                        break;

                    case 4:
                        lignoBinCaptures.RegisterFlowBinTestResults(lblFlowBinNumber.Text,
                                                                    "0",
                                                                    DateTime.Now.ToString(),
                                                                    DateTime.Now.ToString(),
                                                                    DateTime.Now.ToString(),
                                                                    Testno,
                                                                    Convert.ToDecimal(txtDensity.Text),
                                                                    Convert.ToDecimal(txtDensityMin.Text),
                                                                    Convert.ToDecimal(txtDensityMax.Text),
                                                                    Convert.ToDecimal(txtDensityTarget.Text),
                                                                    Convert.ToDecimal(txtDensityLrl.Text),
                                                                    Convert.ToDecimal(txtDensityUrl.Text),
                                                                    "0",
                                                                    Samplepoint);
                        break;

                    default:
                        break;
                }
            }

        }
        #endregion

        #region Select product from list in cmbproduct
        private void cmbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            //btnCHKManualTests.Enabled = false;

            cmbProduct.DropDownStyle = ComboBoxStyle.DropDownList;
            if (cmbProductsLoaded == true)
            {
                try

                {

                    if (lignoBinCaptures.AuthBoxStateProduct)
                    { 
                        CallAuthForProductOutOfSpecTests();
                        btnCHKManualTests.Enabled = false;
                    }
                    else
                    {
                        bnt_ProcessBin.Enabled = true;
                        btnCHKManualTests.Enabled = false;
                    }

                    if (!CheckLignoProductSpecs())
                    {
                        bnt_ProcessBin.Enabled = true;
                       btnCHKManualTests.Enabled = false;
                    }


                }
                catch (Exception ex)
                {
                    // DisableControls_When_Lis_Load_Fails();
                    MessageBox.Show("" + ex.Message);
                    Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath, Properties.Settings.Default.ErrorLogloc, DateTime.Now + " --> " + ex.Message + " " + ex.TargetSite);
                }
            }

        }
        #endregion

        #region calls procedure to start the auth for reprint and display reprint dialog 
        private void bntReprint_Click(object sender, EventArgs e)
        {
            AuthProcedureForReprint();
        }
        #endregion

        #region Auth procedure for reprint
        private void AuthProcedureForReprint()
        {
            if (lignoBinCaptures.GetAuthBoxReprintState())
            {
                GetAuthDetails();

                if (AuthSuccess)
                {
                    using (var reprint = new RePrint_Form.frmReprintLabels())
                    {
                        reprint.ShowDialog();
                        lignoBinCaptures.Authlog(AuthBoxCodeCheck, EmpNo, Username, Reason);
                    }
                }
            }
            else
            {
                using (var reprint = new RePrint_Form.frmReprintLabels())
                {
                    reprint.ShowDialog();
                }
            }
        }
        #endregion

        #region for testing- used to input values into the unloaded, loaded and litres boxes
        private void button1_Click(object sender, EventArgs e)
        {
            txtUnloaded.Text = "10";
            txtLoaded.Text = "1000";
            txtLiters.Text = "1000";
        }
        #endregion

        #region Commit changes to ligno flowbin tables
        private void bnt_ProcessBin_Click(object sender, EventArgs e)
        {
           
            Register();

        }
        #endregion

        #region To enable and disable Litres
        private void chkScaleLtr_MouseClick(object sender, MouseEventArgs e)
        {

            GetAuthDetails();

            if (AuthSuccess == true)
            {
                lignoBinCaptures.UpdateScaleActiveStates(scaleCodeLtr);
                ScaleInputState(scaleCodeLtr);
                lignoBinCaptures.Authlog(scaleCodeLtr, EmpNo, Username, Reason);
            }

            ScaleInputState(scaleCodeLtr);
            CleanAuthBoxDetails();
        }
        #endregion

        #region Clean up auth details
        private void CleanAuthBoxDetails()
        {
            Username = "";
            EmpNo = "";
            Reason = "";
            AuthSuccess = false;
        }
        #endregion

        #region To enable and disable Mass rounding
        private void chkMassRound_MouseClick(object sender, MouseEventArgs e)
        {

            GetAuthDetails();

            if (AuthSuccess == true)
            {
                lignoBinCaptures.UpdateScaleActiveStates(scaleCodeMRO);
                ScaleInputState(scaleCodeMRO);
                lignoBinCaptures.Authlog(scaleCodeMRO, EmpNo, Username, Reason);
            }
            ScaleInputState(scaleCodeMRO);
            CleanAuthBoxDetails();
        }
        #endregion

        #region Gets auth Details from auth box for updating mill transaction and authlog
        private void GetAuthDetails()
        {
            using (LignoBinCapture.AuthBox.AuthBox authbox = new LignoBinCapture.AuthBox.AuthBox())
            {
                authbox.ShowDialog();
                Username = authbox.username;
                EmpNo = authbox.empno;
                Reason = authbox.Reasontxt;
                AuthSuccess = authbox.Reasonsuccess;
                lignoBinCaptures.Username = Username;
                lignoBinCaptures.EmpNo = EmpNo;
                lignoBinCaptures.ReasonDetails = Reason;
            }
        }
        #endregion

        #region Gets the new scale settings user inputs and updates tbl_parameters
        private void ProcessUpdatedScaleRangeSettings()
        {
            foreach (DataRow dt in ProcessUpdatedScaleSettings.Rows)
            {

                switch (dt["param_code"].ToString())
                {
                    case scaleCodeLtr:
                        lignoBinCaptures.UpdateParamSettings(dt["param_code"].ToString(),
                                                            int.Parse(dt["Maxval"].ToString()),
                                                            int.Parse(dt["Minval"].ToString()),
                                                            int.Parse(dt["Round"].ToString()));
                        break;

                    case scaleCodeUnloadedCheck:
                        lignoBinCaptures.UpdateParamSettings(dt["param_code"].ToString(),
                                                              int.Parse(dt["Maxval"].ToString()),
                                                              int.Parse(dt["Minval"].ToString()),
                                                              int.Parse(dt["Round"].ToString()));
                        break;

                    case scaleCodeLoadedCheck:
                        lignoBinCaptures.UpdateParamSettings(dt["param_code"].ToString(),
                                                            int.Parse(dt["Maxval"].ToString()),
                                                            int.Parse(dt["Minval"].ToString()),
                                                            int.Parse(dt["Round"].ToString()));
                        break;
                    case scaleCodeMRO:
                        lignoBinCaptures.UpdateParamSettings(dt["param_code"].ToString(),
                                                            int.Parse(dt["Maxval"].ToString()),
                                                            int.Parse(dt["Minval"].ToString()),
                                                            int.Parse(dt["Round"].ToString()));
                        break;

                    default:
                        break;

                }
            }
        }
        #endregion

        #region Show scale settings when scale settings button is pressed 
        private Boolean ShowScaleSettings()
        {
            Boolean check = false;
            using (var scs = new Scale_Settings_Form.frmScaleSettings())
            {
                scs.ShowDialog();
                if (scs.CheckState == true)
                {
                    ProcessUpdatedScaleSettings = scs.updatedtable;
                    ProcessUpdatedScaleRangeSettings();
                    check = true;
                }
                return check;
            }
        }
        #endregion

        #region Allows the user to make changes to scale settings
        private void bntScaleSettings_Click(object sender, EventArgs e)
        {
            try
            {
                GetAuthDetails();

                if (AuthSuccess)
                {
                    if (ShowScaleSettings())
                    {
                        error_Label.ForeColor = Color.Green;
                        error_Label.Text = _msg_saved;
                        Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                           Properties.Settings.Default.ErrorLogloc,
                                           DateTime.Now +
                                           " --> " +
                                           _msg_saved);
                    }

                }
            }
            catch (Exception ex)
            {
                error_Label.ForeColor = Color.Crimson;
                error_Label.Text = _error_problem_saving + ex.Message;
                MessageBox.Show(_error_problem_saving + ex.Message,
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath,
                                   Properties.Settings.Default.ErrorLogloc,
                                   DateTime.Now +
                                   " --> " +
                                   "Error" +
                                   _error_problem_saving +
                                   ex.Message +
                                   " " +
                                   ex.TargetSite);
            }
        }
        #endregion

        #region Check is Done in pck_flowbin_capture and highlights the txtbox green or red if in or out of spec
        private Boolean CheckLignoLiquidSpecs()
        {
            Boolean check = false;
            if (lignoBinCaptures.CheckLignoLiquidSpecs(decimal.Parse(txtPh.Text), 1, cmbTanks.Text))
                txtPh.BackColor = Color.LightGreen;
            else
            {
                txtPh.BackColor = Color.LightPink;
                check = true;
            }
            if (lignoBinCaptures.CheckLignoLiquidSpecs(decimal.Parse(txtSolids.Text), 2, cmbTanks.Text))
                txtSolids.BackColor = Color.LightGreen;
            else
            {
                txtSolids.BackColor = Color.LightPink;
                check = true;
            }
            if (lignoBinCaptures.CheckLignoLiquidSpecs(decimal.Parse(txtInsolubles.Text), 3, cmbTanks.Text))
                txtInsolubles.BackColor = Color.LightGreen;
            else
            {
                txtInsolubles.BackColor = Color.LightPink;
                check = true;
            }
            if (lignoBinCaptures.CheckLignoLiquidSpecs(decimal.Parse(txtDensity.Text), 4, cmbTanks.Text))
                txtDensity.BackColor = Color.LightGreen;
            else
            {
                txtDensity.BackColor = Color.LightPink;
                check = true;
            }
            return check;
        }
        #endregion

        #region Check is Done and highlights the txtbox green or red if in or out of spec
        private Boolean CheckLignoProductSpecs()
        {
            Boolean check = false;
            if (lignoBinCaptures.CheckLignoProductSpecs(decimal.Parse(txtPh.Text), 1, cmbProduct.Text))
                txtPh.BackColor = Color.LightGreen;
            else
            {
                txtPh.BackColor = Color.LightPink;
                check = true;
            }
            if (lignoBinCaptures.CheckLignoProductSpecs(decimal.Parse(txtSolids.Text), 2, cmbProduct.Text))
                txtSolids.BackColor = Color.LightGreen;
            else
            {
                txtSolids.BackColor = Color.LightPink;
                check = true;
            }
            if (lignoBinCaptures.CheckLignoProductSpecs(decimal.Parse(txtInsolubles.Text), 3, cmbProduct.Text))
                txtInsolubles.BackColor = Color.LightGreen;
            else
            {
                txtInsolubles.BackColor = Color.LightPink;
                check = true;
            }
            if (lignoBinCaptures.CheckLignoProductSpecs(decimal.Parse(txtDensity.Text), 4, cmbProduct.Text))
                txtDensity.BackColor = Color.LightGreen;
            else
            {
                txtDensity.BackColor = Color.LightPink;
                check = true;
            }
            return check;
        }
        #endregion


        #region Get Scale Ranges to display when weight amounts are not in Range
        private void PopulateScaleRanges()
        {
            foreach (DataRow dt in lignoBinCaptures.GetScaleParametersForSettings().Rows)
            {

                switch (dt["param_code"].ToString())
                {
                    case scaleCodeLtr:
                        RangeLtrMax = dt["Maxval"].ToString();
                        RangeLtrMin = dt["Minval"].ToString();
                        break;

                    case scaleCodeUnloadedCheck:
                        RangeUnloadedMax = dt["Maxval"].ToString();
                        RangeUnloadedMin = dt["Minval"].ToString();
                        break;

                    case scaleCodeLoadedCheck:
                        RangeLoadedMax = dt["Maxval"].ToString();
                        RangeLoadedMin = dt["Minval"].ToString();
                        break;

                    default:
                        break;

                }
            }
        }

        #endregion

        //******************************AutoScale*****************************

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _spManager.Dispose();
        }

        void _spManager_NewSerialDataRecieved(object sender, SerialDataEventArgs e)
        {
            //int v;
            if (this.InvokeRequired)
            {
                // Using this.Invoke causes deadlock when closing serial port, and BeginInvoke is good practice anyway.
                this.BeginInvoke(new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved), new object[] { sender, e });
                return;
            }

            //int maxTextLength = 1000; // maximum text length in text box
            //if (lblScale. > maxTextLength)
            //    tbData.Text = tbData.Text.Remove(0, tbData.TextLength - maxTextLength);

            // This application is connected to a GPS sending ASCCI characters, so data is converted to text
            string str = Encoding.ASCII.GetString(e.Data);

            // string test = str.Substring(str.Length - 10);
            try
            {
                lblScale.Text = Convert.ToInt32(Regex.Replace(str, @"[^\d]", "").Trim()).ToString();
                //str = (str).ToUpper().Replace("WW", "").Replace("KG", "");
                // lblScale.Text = Regex.Replace(str, @"[^\d]", "").Trim();
            }
            catch (Exception)
            {


            }
        }

        // Handles the "Start Listening"-buttom click event
        private void btnStart_Click(object sender, EventArgs e)
        {
            _spManager.StartListening();
        }

        // Handles the "Stop Listening"-buttom click event
        private void btnStop_Click(object sender, EventArgs e)
        {
            _spManager.StopListening();
        }

        private void UserInitialization()
        {
            _spManager = new SerialPortManager();
            SerialSettings mySerialSettings = _spManager.CurrentSerialSettings;
            //serialSettingsBindingSource.DataSource = mySerialSettings;
            //portNameComboBox.DataSource = mySerialSettings.PortNameCollection;
            //baudRateComboBox.DataSource = mySerialSettings.BaudRateCollection;
            //dataBitsComboBox.DataSource = mySerialSettings.DataBitsCollection;
            //parityComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.Parity));
            //stopBitsComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.StopBits));

            _spManager.NewSerialDataRecieved += new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved);
            this.FormClosing += new FormClosingEventHandler(MainForm_FormClosing);
        }

        private void chkEnableNetMassCalc_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkManualCapture_MouseClick(object sender, MouseEventArgs e)
        {
            GetAuthDetails();

            if (AuthSuccess == true)
            {
                lignoBinCaptures.UpdateScaleActiveStates(ManualTestCapture);
                ScaleInputState(scaleNetMasscalcCode);
                lignoBinCaptures.Authlog(ManualTestCapture, EmpNo, Username, Reason);
            }

            //ManualTestTxtBoxState();
            ScaleInputState(ManualTestCapture);
            CleanAuthBoxDetails();
        }
       

        private void EnableForManualTestCapture()
        {
            txtPh.ReadOnly =false;
            txtSolids.ReadOnly = false;
            txtInsolubles.ReadOnly = false;
            txtDensity.ReadOnly = false;
            

            txtPh.Text = "0";
            txtPhMin.Text = "0";
            txtPhMax.Text = "0";
            txtPhTarget.Text = "0";
            txtPhLrl.Text = "0";
            txtPhUrl.Text = "0";

            txtSolids.Text="0";
            txtSolidsMin.Text = "0";
            txtSolidsMax.Text = "0";
            txtSolidsTarget.Text = "0";
            txtSolidsLrl.Text = "0";
            txtSolidsUrl.Text = "0";
            
            txtInsolubles.Text = "0";
            txtInsolublesMin.Text = "0";
            txtInsolublesMax.Text = "0";
            txtInsolublesTarget.Text = "0";
            txtInsolublesLrl.Text = "0";
            txtInsolublesUrl.Text = "0";

            txtDensity.Text = "0";
            txtDensityMin.Text= "0";
            txtDensityMax.Text = "0";
            txtDensityTarget.Text = "0";
            txtDensityLrl.Text = "0";
            txtDensityUrl.Text = "0";

        }

        private void DisableForAutoTestCapture()
        {
            txtPh.ReadOnly = true;
            txtSolids.ReadOnly = true;
            txtInsolubles.ReadOnly = true;
            txtDensity.ReadOnly = true;
            
            //txtPh.Text = "0";
            //txtPhMin.Text = "0";
            //txtPhMax.Text = "0";
            //txtPhTarget.Text = "0";
            //txtPhLrl.Text = "0";
            //txtPhUrl.Text = "0";

            //txtSolids.Text = "0";
            //txtSolidsMin.Text = "0";
            //txtSolidsMax.Text = "0";
            //txtSolidsTarget.Text = "0";
            //txtSolidsLrl.Text = "0";
            //txtSolidsUrl.Text = "0";

            //txtInsolubles.Text = "0";
            //txtInsolublesMin.Text = "0";
            //txtInsolublesMax.Text = "0";
            //txtInsolublesTarget.Text = "0";
            //txtInsolublesLrl.Text = "0";
            //txtInsolublesUrl.Text = "0";

            //txtDensity.Text = "0";
            //txtDensityMin.Text = "0";
            //txtDensityMax.Text = "0";
            //txtDensityTarget.Text = "0";
            //txtDensityLrl.Text = "0";
            //txtDensityUrl.Text = "0";

        }

       
    }
}
   
