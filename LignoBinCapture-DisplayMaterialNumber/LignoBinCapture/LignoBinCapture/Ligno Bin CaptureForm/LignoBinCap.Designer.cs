﻿namespace LignoBinCapture
{
    partial class LignoBinCap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LignoBinCap));
            this.bnt_ProcessBin = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lnkLblOpenErrorLog = new System.Windows.Forms.LinkLabel();
            this.error_Label = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbProduct = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.lbltank = new System.Windows.Forms.Label();
            this.cmbTanks = new System.Windows.Forms.ComboBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.btnCHKManualTests = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDensityUrl = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtInsolublesUrl = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSolidsUrl = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtPhUrl = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtDensityLrl = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtInsolublesLrl = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSolidsLrl = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPhLrl = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDensityTarget = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtInsolublesTarget = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSolidsTarget = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPhTarget = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDensityMax = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtInsolublesMax = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSolidsMax = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPhMax = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDensityMin = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtInsolublesMin = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSolidsMin = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPhMin = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDensity = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtInsolubles = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSolids = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPh = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkSpecialTest = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.chkEnableNetMassCalc = new System.Windows.Forms.CheckBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.chkMassRound = new System.Windows.Forms.CheckBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.chkScaleLtr = new System.Windows.Forms.CheckBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.chkScalelState = new System.Windows.Forms.CheckBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.chkManualCapture = new System.Windows.Forms.CheckBox();
            this.lblScale = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblFlowBinNumber = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUnloaded = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLoaded = new System.Windows.Forms.TextBox();
            this.lblLoadedLtr = new System.Windows.Forms.Label();
            this.txtLiters = new System.Windows.Forms.TextBox();
            this.lblNettMass = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.bntScale = new System.Windows.Forms.Button();
            this.bnt_Clear = new System.Windows.Forms.Button();
            this.bnt_Exit = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbl_Yesterday = new System.Windows.Forms.Label();
            this.dataGrid_Yesterday = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lbl_Shift = new System.Windows.Forms.Label();
            this.dataGrid_Shift = new System.Windows.Forms.DataGridView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.bntScaleSettings = new System.Windows.Forms.Button();
            this.bntReprint = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_Today = new System.Windows.Forms.Label();
            this.dataGrid_Scanned = new System.Windows.Forms.DataGridView();
            this.s_Strip = new System.Windows.Forms.StatusStrip();
            this.tsslLiveOrDev = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbl_DateTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbl_StationCode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Yesterday)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Shift)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Scanned)).BeginInit();
            this.s_Strip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bnt_ProcessBin
            // 
            this.bnt_ProcessBin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_ProcessBin.Location = new System.Drawing.Point(10, 20);
            this.bnt_ProcessBin.Name = "bnt_ProcessBin";
            this.bnt_ProcessBin.Size = new System.Drawing.Size(166, 75);
            this.bnt_ProcessBin.TabIndex = 1;
            this.bnt_ProcessBin.Text = "Register Bin";
            this.bnt_ProcessBin.UseVisualStyleBackColor = true;
            this.bnt_ProcessBin.Click += new System.EventHandler(this.bnt_ProcessBin_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lnkLblOpenErrorLog);
            this.groupBox1.Controls.Add(this.error_Label);
            this.groupBox1.Location = new System.Drawing.Point(537, 60);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(401, 222);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Messages";
            // 
            // lnkLblOpenErrorLog
            // 
            this.lnkLblOpenErrorLog.AutoSize = true;
            this.lnkLblOpenErrorLog.Location = new System.Drawing.Point(290, 198);
            this.lnkLblOpenErrorLog.Name = "lnkLblOpenErrorLog";
            this.lnkLblOpenErrorLog.Size = new System.Drawing.Size(105, 15);
            this.lnkLblOpenErrorLog.TabIndex = 14;
            this.lnkLblOpenErrorLog.TabStop = true;
            this.lnkLblOpenErrorLog.Text = "Open Error Log";
            this.lnkLblOpenErrorLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLblOpenErrorLog_LinkClicked);
            // 
            // error_Label
            // 
            this.error_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_Label.Location = new System.Drawing.Point(6, 17);
            this.error_Label.Name = "error_Label";
            this.error_Label.Size = new System.Drawing.Size(389, 199);
            this.error_Label.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel3);
            this.groupBox3.Controls.Add(this.groupBox13);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(6, 539);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1631, 322);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ligno Flow Bin Infomation";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.65625F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.34375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 761F));
            this.tableLayoutPanel3.Controls.Add(this.cmbProduct, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label31, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.lbltank, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmbTanks, 0, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(13, 20);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1300, 71);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // cmbProduct
            // 
            this.cmbProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProduct.Enabled = false;
            this.cmbProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProduct.FormattingEnabled = true;
            this.cmbProduct.Location = new System.Drawing.Point(541, 26);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Size = new System.Drawing.Size(481, 37);
            this.cmbProduct.TabIndex = 1;
            this.cmbProduct.SelectedIndexChanged += new System.EventHandler(this.cmbProduct_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(541, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(120, 15);
            this.label31.TabIndex = 1;
            this.label31.Text = "Product Selection";
            // 
            // lbltank
            // 
            this.lbltank.AutoSize = true;
            this.lbltank.Location = new System.Drawing.Point(3, 0);
            this.lbltank.Name = "lbltank";
            this.lbltank.Size = new System.Drawing.Size(102, 15);
            this.lbltank.TabIndex = 0;
            this.lbltank.Text = "Tank Selection";
            // 
            // cmbTanks
            // 
            this.cmbTanks.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTanks.Enabled = false;
            this.cmbTanks.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTanks.FormattingEnabled = true;
            this.cmbTanks.Location = new System.Drawing.Point(3, 26);
            this.cmbTanks.Name = "cmbTanks";
            this.cmbTanks.Size = new System.Drawing.Size(520, 37);
            this.cmbTanks.TabIndex = 2;
            this.cmbTanks.SelectedIndexChanged += new System.EventHandler(this.cmbTanks_SelectedIndexChanged);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.btnCHKManualTests);
            this.groupBox13.Controls.Add(this.tableLayoutPanel4);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.Location = new System.Drawing.Point(6, 111);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(1619, 193);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Ligno Liquid LIS Information";
            // 
            // btnCHKManualTests
            // 
            this.btnCHKManualTests.Enabled = false;
            this.btnCHKManualTests.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCHKManualTests.Location = new System.Drawing.Point(1193, 16);
            this.btnCHKManualTests.Name = "btnCHKManualTests";
            this.btnCHKManualTests.Size = new System.Drawing.Size(154, 169);
            this.btnCHKManualTests.TabIndex = 9;
            this.btnCHKManualTests.Text = "Check Manual Tests";
            this.btnCHKManualTests.UseVisualStyleBackColor = true;
            this.btnCHKManualTests.Visible = false;
            this.btnCHKManualTests.Click += new System.EventHandler(this.btnCHKManualTests_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 12;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.55814F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.69767F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.16279F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.09302F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.7931F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.txtDensityUrl, 10, 8);
            this.tableLayoutPanel4.Controls.Add(this.label26, 10, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtInsolublesUrl, 10, 6);
            this.tableLayoutPanel4.Controls.Add(this.label25, 10, 5);
            this.tableLayoutPanel4.Controls.Add(this.txtSolidsUrl, 10, 4);
            this.tableLayoutPanel4.Controls.Add(this.label24, 10, 3);
            this.tableLayoutPanel4.Controls.Add(this.txtPhUrl, 10, 2);
            this.tableLayoutPanel4.Controls.Add(this.label23, 10, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtDensityLrl, 8, 8);
            this.tableLayoutPanel4.Controls.Add(this.label22, 8, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtInsolublesLrl, 8, 6);
            this.tableLayoutPanel4.Controls.Add(this.label21, 8, 5);
            this.tableLayoutPanel4.Controls.Add(this.txtSolidsLrl, 8, 4);
            this.tableLayoutPanel4.Controls.Add(this.label20, 8, 3);
            this.tableLayoutPanel4.Controls.Add(this.txtPhLrl, 8, 2);
            this.tableLayoutPanel4.Controls.Add(this.label19, 8, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtDensityTarget, 6, 8);
            this.tableLayoutPanel4.Controls.Add(this.label18, 6, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtInsolublesTarget, 6, 6);
            this.tableLayoutPanel4.Controls.Add(this.label17, 6, 5);
            this.tableLayoutPanel4.Controls.Add(this.txtSolidsTarget, 6, 4);
            this.tableLayoutPanel4.Controls.Add(this.label16, 6, 3);
            this.tableLayoutPanel4.Controls.Add(this.txtPhTarget, 6, 2);
            this.tableLayoutPanel4.Controls.Add(this.label15, 6, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtDensityMax, 4, 8);
            this.tableLayoutPanel4.Controls.Add(this.label14, 4, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtInsolublesMax, 4, 6);
            this.tableLayoutPanel4.Controls.Add(this.label13, 4, 5);
            this.tableLayoutPanel4.Controls.Add(this.txtSolidsMax, 4, 4);
            this.tableLayoutPanel4.Controls.Add(this.label12, 4, 3);
            this.tableLayoutPanel4.Controls.Add(this.txtPhMax, 4, 2);
            this.tableLayoutPanel4.Controls.Add(this.label11, 4, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtDensityMin, 2, 8);
            this.tableLayoutPanel4.Controls.Add(this.label10, 2, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtInsolublesMin, 2, 6);
            this.tableLayoutPanel4.Controls.Add(this.label9, 2, 5);
            this.tableLayoutPanel4.Controls.Add(this.txtSolidsMin, 2, 4);
            this.tableLayoutPanel4.Controls.Add(this.label8, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.txtPhMin, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.label7, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtDensity, 0, 8);
            this.tableLayoutPanel4.Controls.Add(this.label6, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtInsolubles, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.txtSolids, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.txtPh, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(4, 20);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 9;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1177, 169);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // txtDensityUrl
            // 
            this.txtDensityUrl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtDensityUrl.Location = new System.Drawing.Point(1018, 144);
            this.txtDensityUrl.Name = "txtDensityUrl";
            this.txtDensityUrl.ReadOnly = true;
            this.txtDensityUrl.Size = new System.Drawing.Size(154, 21);
            this.txtDensityUrl.TabIndex = 3;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(1018, 126);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 15);
            this.label26.TabIndex = 7;
            this.label26.Text = "Density URL";
            // 
            // txtInsolublesUrl
            // 
            this.txtInsolublesUrl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtInsolublesUrl.Location = new System.Drawing.Point(1018, 102);
            this.txtInsolublesUrl.Name = "txtInsolublesUrl";
            this.txtInsolublesUrl.ReadOnly = true;
            this.txtInsolublesUrl.Size = new System.Drawing.Size(154, 21);
            this.txtInsolublesUrl.TabIndex = 2;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(1018, 84);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(105, 15);
            this.label25.TabIndex = 6;
            this.label25.Text = "Insolubles URL";
            // 
            // txtSolidsUrl
            // 
            this.txtSolidsUrl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtSolidsUrl.Location = new System.Drawing.Point(1018, 60);
            this.txtSolidsUrl.Name = "txtSolidsUrl";
            this.txtSolidsUrl.ReadOnly = true;
            this.txtSolidsUrl.Size = new System.Drawing.Size(154, 21);
            this.txtSolidsUrl.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(1018, 42);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 15);
            this.label24.TabIndex = 5;
            this.label24.Text = "Solids URL";
            // 
            // txtPhUrl
            // 
            this.txtPhUrl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtPhUrl.Location = new System.Drawing.Point(1018, 18);
            this.txtPhUrl.Name = "txtPhUrl";
            this.txtPhUrl.ReadOnly = true;
            this.txtPhUrl.Size = new System.Drawing.Size(154, 21);
            this.txtPhUrl.TabIndex = 0;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(1018, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 15);
            this.label23.TabIndex = 4;
            this.label23.Text = "PH URL";
            // 
            // txtDensityLrl
            // 
            this.txtDensityLrl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtDensityLrl.Location = new System.Drawing.Point(829, 144);
            this.txtDensityLrl.Name = "txtDensityLrl";
            this.txtDensityLrl.ReadOnly = true;
            this.txtDensityLrl.Size = new System.Drawing.Size(154, 21);
            this.txtDensityLrl.TabIndex = 3;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(829, 126);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 15);
            this.label22.TabIndex = 7;
            this.label22.Text = "Density LRL";
            // 
            // txtInsolublesLrl
            // 
            this.txtInsolublesLrl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtInsolublesLrl.Location = new System.Drawing.Point(829, 102);
            this.txtInsolublesLrl.Name = "txtInsolublesLrl";
            this.txtInsolublesLrl.ReadOnly = true;
            this.txtInsolublesLrl.Size = new System.Drawing.Size(154, 21);
            this.txtInsolublesLrl.TabIndex = 2;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(829, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(103, 15);
            this.label21.TabIndex = 6;
            this.label21.Text = "Insolubles LRL";
            // 
            // txtSolidsLrl
            // 
            this.txtSolidsLrl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtSolidsLrl.Location = new System.Drawing.Point(829, 60);
            this.txtSolidsLrl.Name = "txtSolidsLrl";
            this.txtSolidsLrl.ReadOnly = true;
            this.txtSolidsLrl.Size = new System.Drawing.Size(154, 21);
            this.txtSolidsLrl.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(829, 42);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 15);
            this.label20.TabIndex = 5;
            this.label20.Text = "Solids LRL";
            // 
            // txtPhLrl
            // 
            this.txtPhLrl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtPhLrl.Location = new System.Drawing.Point(829, 18);
            this.txtPhLrl.Name = "txtPhLrl";
            this.txtPhLrl.ReadOnly = true;
            this.txtPhLrl.Size = new System.Drawing.Size(154, 21);
            this.txtPhLrl.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(829, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 15);
            this.label19.TabIndex = 4;
            this.label19.Text = "PH LRL";
            // 
            // txtDensityTarget
            // 
            this.txtDensityTarget.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtDensityTarget.Location = new System.Drawing.Point(643, 144);
            this.txtDensityTarget.Name = "txtDensityTarget";
            this.txtDensityTarget.ReadOnly = true;
            this.txtDensityTarget.Size = new System.Drawing.Size(154, 21);
            this.txtDensityTarget.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(643, 126);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 15);
            this.label18.TabIndex = 7;
            this.label18.Text = "DensityTarget";
            // 
            // txtInsolublesTarget
            // 
            this.txtInsolublesTarget.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtInsolublesTarget.Location = new System.Drawing.Point(643, 102);
            this.txtInsolublesTarget.Name = "txtInsolublesTarget";
            this.txtInsolublesTarget.ReadOnly = true;
            this.txtInsolublesTarget.Size = new System.Drawing.Size(154, 21);
            this.txtInsolublesTarget.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(643, 84);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(118, 15);
            this.label17.TabIndex = 6;
            this.label17.Text = "Insolubles Target";
            // 
            // txtSolidsTarget
            // 
            this.txtSolidsTarget.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtSolidsTarget.Location = new System.Drawing.Point(643, 60);
            this.txtSolidsTarget.Name = "txtSolidsTarget";
            this.txtSolidsTarget.ReadOnly = true;
            this.txtSolidsTarget.Size = new System.Drawing.Size(154, 21);
            this.txtSolidsTarget.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(643, 42);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 15);
            this.label16.TabIndex = 5;
            this.label16.Text = "Solids Target";
            // 
            // txtPhTarget
            // 
            this.txtPhTarget.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtPhTarget.Location = new System.Drawing.Point(643, 18);
            this.txtPhTarget.Name = "txtPhTarget";
            this.txtPhTarget.ReadOnly = true;
            this.txtPhTarget.Size = new System.Drawing.Size(154, 21);
            this.txtPhTarget.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(643, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 15);
            this.label15.TabIndex = 4;
            this.label15.Text = "PH Target";
            // 
            // txtDensityMax
            // 
            this.txtDensityMax.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtDensityMax.Location = new System.Drawing.Point(459, 144);
            this.txtDensityMax.Name = "txtDensityMax";
            this.txtDensityMax.ReadOnly = true;
            this.txtDensityMax.Size = new System.Drawing.Size(154, 21);
            this.txtDensityMax.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(459, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 15);
            this.label14.TabIndex = 7;
            this.label14.Text = "Density Max";
            // 
            // txtInsolublesMax
            // 
            this.txtInsolublesMax.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtInsolublesMax.Location = new System.Drawing.Point(459, 102);
            this.txtInsolublesMax.Name = "txtInsolublesMax";
            this.txtInsolublesMax.ReadOnly = true;
            this.txtInsolublesMax.Size = new System.Drawing.Size(154, 21);
            this.txtInsolublesMax.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(459, 84);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 15);
            this.label13.TabIndex = 6;
            this.label13.Text = "Insolubles Max";
            // 
            // txtSolidsMax
            // 
            this.txtSolidsMax.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtSolidsMax.Location = new System.Drawing.Point(459, 60);
            this.txtSolidsMax.Name = "txtSolidsMax";
            this.txtSolidsMax.ReadOnly = true;
            this.txtSolidsMax.Size = new System.Drawing.Size(154, 21);
            this.txtSolidsMax.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(459, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 15);
            this.label12.TabIndex = 5;
            this.label12.Text = "Solids Max";
            // 
            // txtPhMax
            // 
            this.txtPhMax.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtPhMax.Location = new System.Drawing.Point(459, 18);
            this.txtPhMax.Name = "txtPhMax";
            this.txtPhMax.ReadOnly = true;
            this.txtPhMax.Size = new System.Drawing.Size(154, 21);
            this.txtPhMax.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(459, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 15);
            this.label11.TabIndex = 4;
            this.label11.Text = "PH Max";
            // 
            // txtDensityMin
            // 
            this.txtDensityMin.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtDensityMin.Location = new System.Drawing.Point(276, 144);
            this.txtDensityMin.Name = "txtDensityMin";
            this.txtDensityMin.ReadOnly = true;
            this.txtDensityMin.Size = new System.Drawing.Size(154, 21);
            this.txtDensityMin.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(276, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 15);
            this.label10.TabIndex = 7;
            this.label10.Text = "Density Min";
            // 
            // txtInsolublesMin
            // 
            this.txtInsolublesMin.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtInsolublesMin.Location = new System.Drawing.Point(276, 102);
            this.txtInsolublesMin.Name = "txtInsolublesMin";
            this.txtInsolublesMin.ReadOnly = true;
            this.txtInsolublesMin.Size = new System.Drawing.Size(154, 21);
            this.txtInsolublesMin.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(276, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 15);
            this.label9.TabIndex = 6;
            this.label9.Text = "Insolubles Min";
            // 
            // txtSolidsMin
            // 
            this.txtSolidsMin.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtSolidsMin.Location = new System.Drawing.Point(276, 60);
            this.txtSolidsMin.Name = "txtSolidsMin";
            this.txtSolidsMin.ReadOnly = true;
            this.txtSolidsMin.Size = new System.Drawing.Size(154, 21);
            this.txtSolidsMin.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(276, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 15);
            this.label8.TabIndex = 5;
            this.label8.Text = "Solids Min";
            // 
            // txtPhMin
            // 
            this.txtPhMin.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtPhMin.Location = new System.Drawing.Point(276, 18);
            this.txtPhMin.Name = "txtPhMin";
            this.txtPhMin.ReadOnly = true;
            this.txtPhMin.Size = new System.Drawing.Size(154, 21);
            this.txtPhMin.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(276, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 15);
            this.label7.TabIndex = 4;
            this.label7.Text = "PH Min";
            // 
            // txtDensity
            // 
            this.txtDensity.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtDensity.Location = new System.Drawing.Point(3, 144);
            this.txtDensity.Name = "txtDensity";
            this.txtDensity.ReadOnly = true;
            this.txtDensity.Size = new System.Drawing.Size(154, 21);
            this.txtDensity.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 15);
            this.label6.TabIndex = 7;
            this.label6.Text = "Density";
            // 
            // txtInsolubles
            // 
            this.txtInsolubles.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtInsolubles.Location = new System.Drawing.Point(3, 102);
            this.txtInsolubles.Name = "txtInsolubles";
            this.txtInsolubles.ReadOnly = true;
            this.txtInsolubles.Size = new System.Drawing.Size(154, 21);
            this.txtInsolubles.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "Insolubles";
            // 
            // txtSolids
            // 
            this.txtSolids.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtSolids.Location = new System.Drawing.Point(3, 60);
            this.txtSolids.Name = "txtSolids";
            this.txtSolids.ReadOnly = true;
            this.txtSolids.Size = new System.Drawing.Size(154, 21);
            this.txtSolids.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Solids";
            // 
            // txtPh
            // 
            this.txtPh.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtPh.Location = new System.Drawing.Point(3, 18);
            this.txtPh.Name = "txtPh";
            this.txtPh.ReadOnly = true;
            this.txtPh.Size = new System.Drawing.Size(154, 21);
            this.txtPh.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "PH";
            // 
            // chkSpecialTest
            // 
            this.chkSpecialTest.AutoSize = true;
            this.chkSpecialTest.Location = new System.Drawing.Point(7, 36);
            this.chkSpecialTest.Name = "chkSpecialTest";
            this.chkSpecialTest.Size = new System.Drawing.Size(15, 14);
            this.chkSpecialTest.TabIndex = 4;
            this.chkSpecialTest.UseVisualStyleBackColor = true;
            this.chkSpecialTest.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkSpecialTest_MouseClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox17);
            this.groupBox4.Controls.Add(this.lblScale);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.lblFlowBinNumber);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.groupBox1);
            this.groupBox4.Controls.Add(this.groupBox9);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox4.Location = new System.Drawing.Point(3, 242);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1631, 291);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Weigh Flow Bin";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.tableLayoutPanel2);
            this.groupBox17.Location = new System.Drawing.Point(944, 60);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(678, 222);
            this.groupBox17.TabIndex = 16;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Options";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox14, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBox16, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBox15, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox10, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox11, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox12, 2, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.64103F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.35897F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(506, 122);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.chkEnableNetMassCalc);
            this.groupBox14.Location = new System.Drawing.Point(171, 64);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(162, 55);
            this.groupBox14.TabIndex = 11;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Net Mass Calc";
            // 
            // chkEnableNetMassCalc
            // 
            this.chkEnableNetMassCalc.AutoSize = true;
            this.chkEnableNetMassCalc.Location = new System.Drawing.Point(7, 36);
            this.chkEnableNetMassCalc.Name = "chkEnableNetMassCalc";
            this.chkEnableNetMassCalc.Size = new System.Drawing.Size(15, 14);
            this.chkEnableNetMassCalc.TabIndex = 0;
            this.chkEnableNetMassCalc.UseVisualStyleBackColor = true;
            this.chkEnableNetMassCalc.CheckedChanged += new System.EventHandler(this.chkEnableNetMassCalc_CheckedChanged);
            this.chkEnableNetMassCalc.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkEnableNetMassCalc_MouseClick_1);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.chkMassRound);
            this.groupBox16.Location = new System.Drawing.Point(3, 64);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(162, 55);
            this.groupBox16.TabIndex = 10;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Mass Rounding";
            // 
            // chkMassRound
            // 
            this.chkMassRound.AutoSize = true;
            this.chkMassRound.Location = new System.Drawing.Point(7, 36);
            this.chkMassRound.Name = "chkMassRound";
            this.chkMassRound.Size = new System.Drawing.Size(15, 14);
            this.chkMassRound.TabIndex = 0;
            this.chkMassRound.UseVisualStyleBackColor = true;
            this.chkMassRound.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkMassRound_MouseClick);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.chkScaleLtr);
            this.groupBox15.Location = new System.Drawing.Point(171, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(162, 55);
            this.groupBox15.TabIndex = 9;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Litre Capture";
            // 
            // chkScaleLtr
            // 
            this.chkScaleLtr.AutoSize = true;
            this.chkScaleLtr.Location = new System.Drawing.Point(7, 36);
            this.chkScaleLtr.Name = "chkScaleLtr";
            this.chkScaleLtr.Size = new System.Drawing.Size(15, 14);
            this.chkScaleLtr.TabIndex = 0;
            this.chkScaleLtr.UseVisualStyleBackColor = true;
            this.chkScaleLtr.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkScaleLtr_MouseClick);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.chkScalelState);
            this.groupBox10.Location = new System.Drawing.Point(3, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(162, 55);
            this.groupBox10.TabIndex = 6;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Manual On/OFF";
            // 
            // chkScalelState
            // 
            this.chkScalelState.AutoSize = true;
            this.chkScalelState.Location = new System.Drawing.Point(7, 36);
            this.chkScalelState.Name = "chkScalelState";
            this.chkScalelState.Size = new System.Drawing.Size(15, 14);
            this.chkScalelState.TabIndex = 0;
            this.chkScalelState.UseVisualStyleBackColor = true;
            this.chkScalelState.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkScalelState_MouseClick);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.chkSpecialTest);
            this.groupBox11.Location = new System.Drawing.Point(339, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(164, 55);
            this.groupBox11.TabIndex = 12;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Special Test";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.chkManualCapture);
            this.groupBox12.Location = new System.Drawing.Point(339, 64);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(164, 55);
            this.groupBox12.TabIndex = 13;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Manual Test Capture";
            // 
            // chkManualCapture
            // 
            this.chkManualCapture.AutoSize = true;
            this.chkManualCapture.Location = new System.Drawing.Point(7, 36);
            this.chkManualCapture.Name = "chkManualCapture";
            this.chkManualCapture.Size = new System.Drawing.Size(15, 14);
            this.chkManualCapture.TabIndex = 9;
            this.chkManualCapture.UseVisualStyleBackColor = true;
            this.chkManualCapture.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkManualCapture_MouseClick);
            // 
            // lblScale
            // 
            this.lblScale.AutoSize = true;
            this.lblScale.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScale.ForeColor = System.Drawing.Color.Red;
            this.lblScale.Location = new System.Drawing.Point(84, 27);
            this.lblScale.Name = "lblScale";
            this.lblScale.Size = new System.Drawing.Size(28, 24);
            this.lblScale.TabIndex = 13;
            this.lblScale.Text = "...";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(9, 27);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(86, 24);
            this.label28.TabIndex = 12;
            this.label28.Text = "Scale. : ";
            // 
            // lblFlowBinNumber
            // 
            this.lblFlowBinNumber.AutoSize = true;
            this.lblFlowBinNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFlowBinNumber.ForeColor = System.Drawing.Color.Green;
            this.lblFlowBinNumber.Location = new System.Drawing.Point(673, 33);
            this.lblFlowBinNumber.Name = "lblFlowBinNumber";
            this.lblFlowBinNumber.Size = new System.Drawing.Size(28, 24);
            this.lblFlowBinNumber.TabIndex = 8;
            this.lblFlowBinNumber.Text = "...";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(533, 33);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(148, 24);
            this.label27.TabIndex = 7;
            this.label27.Text = "Flow Bin No. : ";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.flowLayoutPanel7);
            this.groupBox9.Controls.Add(this.lblNettMass);
            this.groupBox9.Controls.Add(this.label29);
            this.groupBox9.Controls.Add(this.bntScale);
            this.groupBox9.Location = new System.Drawing.Point(6, 60);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(525, 222);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.label1);
            this.flowLayoutPanel7.Controls.Add(this.txtUnloaded);
            this.flowLayoutPanel7.Controls.Add(this.label2);
            this.flowLayoutPanel7.Controls.Add(this.txtLoaded);
            this.flowLayoutPanel7.Controls.Add(this.lblLoadedLtr);
            this.flowLayoutPanel7.Controls.Add(this.txtLiters);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(7, 19);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(285, 170);
            this.flowLayoutPanel7.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Unloaded Weight";
            // 
            // txtUnloaded
            // 
            this.txtUnloaded.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnloaded.Location = new System.Drawing.Point(3, 19);
            this.txtUnloaded.Name = "txtUnloaded";
            this.txtUnloaded.Size = new System.Drawing.Size(274, 31);
            this.txtUnloaded.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Loaded Weight";
            // 
            // txtLoaded
            // 
            this.txtLoaded.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoaded.Location = new System.Drawing.Point(3, 72);
            this.txtLoaded.Name = "txtLoaded";
            this.txtLoaded.Size = new System.Drawing.Size(274, 31);
            this.txtLoaded.TabIndex = 1;
            // 
            // lblLoadedLtr
            // 
            this.lblLoadedLtr.AutoSize = true;
            this.lblLoadedLtr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoadedLtr.Location = new System.Drawing.Point(3, 106);
            this.lblLoadedLtr.Name = "lblLoadedLtr";
            this.lblLoadedLtr.Size = new System.Drawing.Size(103, 16);
            this.lblLoadedLtr.TabIndex = 12;
            this.lblLoadedLtr.Text = "Loaded Litres";
            // 
            // txtLiters
            // 
            this.txtLiters.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLiters.Location = new System.Drawing.Point(3, 125);
            this.txtLiters.Name = "txtLiters";
            this.txtLiters.Size = new System.Drawing.Size(274, 31);
            this.txtLiters.TabIndex = 11;
            // 
            // lblNettMass
            // 
            this.lblNettMass.AutoSize = true;
            this.lblNettMass.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNettMass.ForeColor = System.Drawing.Color.Green;
            this.lblNettMass.Location = new System.Drawing.Point(120, 192);
            this.lblNettMass.Name = "lblNettMass";
            this.lblNettMass.Size = new System.Drawing.Size(28, 24);
            this.lblNettMass.TabIndex = 10;
            this.lblNettMass.Text = "...";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(3, 192);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(125, 24);
            this.label29.TabIndex = 9;
            this.label29.Text = "Nett Mass. : ";
            // 
            // bntScale
            // 
            this.bntScale.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntScale.Location = new System.Drawing.Point(333, 20);
            this.bntScale.Name = "bntScale";
            this.bntScale.Size = new System.Drawing.Size(155, 162);
            this.bntScale.TabIndex = 4;
            this.bntScale.Text = "Get Unloaded Weight";
            this.bntScale.UseVisualStyleBackColor = true;
            this.bntScale.Click += new System.EventHandler(this.bntScale_Click);
            // 
            // bnt_Clear
            // 
            this.bnt_Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Clear.Location = new System.Drawing.Point(113, 23);
            this.bnt_Clear.Name = "bnt_Clear";
            this.bnt_Clear.Size = new System.Drawing.Size(101, 75);
            this.bnt_Clear.TabIndex = 7;
            this.bnt_Clear.Text = "Clear";
            this.bnt_Clear.UseVisualStyleBackColor = true;
            this.bnt_Clear.Click += new System.EventHandler(this.bnt_Clear_Click);
            // 
            // bnt_Exit
            // 
            this.bnt_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Exit.Location = new System.Drawing.Point(326, 23);
            this.bnt_Exit.Name = "bnt_Exit";
            this.bnt_Exit.Size = new System.Drawing.Size(101, 75);
            this.bnt_Exit.TabIndex = 8;
            this.bnt_Exit.Text = "Exit";
            this.bnt_Exit.UseVisualStyleBackColor = true;
            this.bnt_Exit.Click += new System.EventHandler(this.bnt_Exit_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbl_Yesterday);
            this.groupBox5.Controls.Add(this.dataGrid_Yesterday);
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(543, 227);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Scanned Bins Yesterday";
            // 
            // lbl_Yesterday
            // 
            this.lbl_Yesterday.AutoSize = true;
            this.lbl_Yesterday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Yesterday.Location = new System.Drawing.Point(10, 16);
            this.lbl_Yesterday.Name = "lbl_Yesterday";
            this.lbl_Yesterday.Size = new System.Drawing.Size(93, 15);
            this.lbl_Yesterday.TabIndex = 4;
            this.lbl_Yesterday.Text = "lbl_Yesterday";
            // 
            // dataGrid_Yesterday
            // 
            this.dataGrid_Yesterday.AllowUserToDeleteRows = false;
            this.dataGrid_Yesterday.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGrid_Yesterday.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_Yesterday.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGrid_Yesterday.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_Yesterday.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGrid_Yesterday.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGrid_Yesterday.Enabled = false;
            this.dataGrid_Yesterday.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGrid_Yesterday.Location = new System.Drawing.Point(13, 34);
            this.dataGrid_Yesterday.MultiSelect = false;
            this.dataGrid_Yesterday.Name = "dataGrid_Yesterday";
            this.dataGrid_Yesterday.Size = new System.Drawing.Size(518, 187);
            this.dataGrid_Yesterday.TabIndex = 2;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lbl_Shift);
            this.groupBox6.Controls.Add(this.dataGrid_Shift);
            this.groupBox6.Location = new System.Drawing.Point(1102, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(540, 227);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Shift Totals";
            // 
            // lbl_Shift
            // 
            this.lbl_Shift.AutoSize = true;
            this.lbl_Shift.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Shift.Location = new System.Drawing.Point(6, 16);
            this.lbl_Shift.Name = "lbl_Shift";
            this.lbl_Shift.Size = new System.Drawing.Size(60, 15);
            this.lbl_Shift.TabIndex = 5;
            this.lbl_Shift.Text = "lbl_Shift";
            // 
            // dataGrid_Shift
            // 
            this.dataGrid_Shift.AllowUserToDeleteRows = false;
            this.dataGrid_Shift.BackgroundColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_Shift.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGrid_Shift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_Shift.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGrid_Shift.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGrid_Shift.GridColor = System.Drawing.Color.LightGreen;
            this.dataGrid_Shift.Location = new System.Drawing.Point(9, 34);
            this.dataGrid_Shift.MultiSelect = false;
            this.dataGrid_Shift.Name = "dataGrid_Shift";
            this.dataGrid_Shift.ReadOnly = true;
            this.dataGrid_Shift.Size = new System.Drawing.Size(522, 187);
            this.dataGrid_Shift.TabIndex = 2;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.bnt_ProcessBin);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox7.Location = new System.Drawing.Point(9, 867);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(186, 107);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Processing Options";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.bnt_Exit);
            this.groupBox8.Controls.Add(this.bntScaleSettings);
            this.groupBox8.Controls.Add(this.bntReprint);
            this.groupBox8.Controls.Add(this.bnt_Clear);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox8.Location = new System.Drawing.Point(201, 867);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(433, 107);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "System Options";
            // 
            // bntScaleSettings
            // 
            this.bntScaleSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.bntScaleSettings.Location = new System.Drawing.Point(220, 23);
            this.bntScaleSettings.Name = "bntScaleSettings";
            this.bntScaleSettings.Size = new System.Drawing.Size(101, 75);
            this.bntScaleSettings.TabIndex = 15;
            this.bntScaleSettings.Text = "Settings";
            this.bntScaleSettings.UseVisualStyleBackColor = true;
            this.bntScaleSettings.Click += new System.EventHandler(this.bntScaleSettings_Click);
            // 
            // bntReprint
            // 
            this.bntReprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntReprint.Location = new System.Drawing.Point(6, 23);
            this.bntReprint.Name = "bntReprint";
            this.bntReprint.Size = new System.Drawing.Size(101, 75);
            this.bntReprint.TabIndex = 9;
            this.bntReprint.Text = "Reprint";
            this.bntReprint.UseVisualStyleBackColor = true;
            this.bntReprint.Click += new System.EventHandler(this.bntReprint_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_Today);
            this.groupBox2.Controls.Add(this.dataGrid_Scanned);
            this.groupBox2.Location = new System.Drawing.Point(552, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(544, 227);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scanned Bins Today";
            // 
            // lbl_Today
            // 
            this.lbl_Today.AutoSize = true;
            this.lbl_Today.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Today.Location = new System.Drawing.Point(6, 16);
            this.lbl_Today.Name = "lbl_Today";
            this.lbl_Today.Size = new System.Drawing.Size(69, 15);
            this.lbl_Today.TabIndex = 3;
            this.lbl_Today.Text = "lbl_Today";
            // 
            // dataGrid_Scanned
            // 
            this.dataGrid_Scanned.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dataGrid_Scanned.AllowUserToDeleteRows = false;
            this.dataGrid_Scanned.AllowUserToResizeColumns = false;
            this.dataGrid_Scanned.AllowUserToResizeRows = false;
            this.dataGrid_Scanned.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGrid_Scanned.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_Scanned.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGrid_Scanned.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_Scanned.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGrid_Scanned.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGrid_Scanned.Enabled = false;
            this.dataGrid_Scanned.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dataGrid_Scanned.Location = new System.Drawing.Point(9, 34);
            this.dataGrid_Scanned.MultiSelect = false;
            this.dataGrid_Scanned.Name = "dataGrid_Scanned";
            this.dataGrid_Scanned.ReadOnly = true;
            this.dataGrid_Scanned.Size = new System.Drawing.Size(525, 187);
            this.dataGrid_Scanned.TabIndex = 2;
            // 
            // s_Strip
            // 
            this.s_Strip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.s_Strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslLiveOrDev,
            this.toolStripStatusLabel2,
            this.tslbl_DateTime,
            this.tslbl_StationCode,
            this.toolStripStatusLabel1});
            this.s_Strip.Location = new System.Drawing.Point(0, 1001);
            this.s_Strip.Name = "s_Strip";
            this.s_Strip.Size = new System.Drawing.Size(1717, 22);
            this.s_Strip.TabIndex = 12;
            this.s_Strip.Text = "statusStrip1";
            // 
            // tsslLiveOrDev
            // 
            this.tsslLiveOrDev.Name = "tsslLiveOrDev";
            this.tsslLiveOrDev.Size = new System.Drawing.Size(114, 17);
            this.tsslLiveOrDev.Text = "Live or dev indicator";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(1324, 17);
            this.toolStripStatusLabel2.Spring = true;
            // 
            // tslbl_DateTime
            // 
            this.tslbl_DateTime.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslbl_DateTime.Name = "tslbl_DateTime";
            this.tslbl_DateTime.Size = new System.Drawing.Size(99, 17);
            this.tslbl_DateTime.Text = "Station Code | ";
            // 
            // tslbl_StationCode
            // 
            this.tslbl_StationCode.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslbl_StationCode.ForeColor = System.Drawing.Color.ForestGreen;
            this.tslbl_StationCode.Name = "tslbl_StationCode";
            this.tslbl_StationCode.Size = new System.Drawing.Size(115, 17);
            this.tslbl_StationCode.Text = "tslbl_StationCode";
            this.tslbl_StationCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(50, 17);
            this.toolStripStatusLabel1.Text = "  |           ";
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(701, 887);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(175, 75);
            this.button1.TabIndex = 14;
            this.button1.Text = "For Testing - input Values into scale weights";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox6, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1641, 233);
            this.tableLayoutPanel1.TabIndex = 15;
            // 
            // LignoBinCap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1717, 1023);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.s_Strip);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LignoBinCap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ligno Flow Bin Capture";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.LignoLignoBinCap_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Yesterday)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Shift)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Scanned)).EndInit();
            this.s_Strip.ResumeLayout(false);
            this.s_Strip.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bnt_ProcessBin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button bnt_Clear;
        private System.Windows.Forms.Button bnt_Exit;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dataGrid_Yesterday;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGrid_Shift;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lbl_Yesterday;
        private System.Windows.Forms.Label lbl_Shift;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbl_Today;
        private System.Windows.Forms.DataGridView dataGrid_Scanned;
        private System.Windows.Forms.StatusStrip s_Strip;
        private System.Windows.Forms.ToolStripStatusLabel tslbl_StationCode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tslbl_DateTime;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPh;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSolids;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtInsolubles;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDensity;
        private System.Windows.Forms.ComboBox cmbTanks;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox chkScalelState;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button bntScale;
        private System.Windows.Forms.TextBox txtUnloaded;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLoaded;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPhMin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSolidsMin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInsolublesMin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDensityMin;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPhMax;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSolidsMax;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtInsolublesMax;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtDensityMax;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPhTarget;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtSolidsTarget;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtInsolublesTarget;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtDensityTarget;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPhLrl;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtSolidsLrl;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtInsolublesLrl;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDensityLrl;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtPhUrl;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtSolidsUrl;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtInsolublesUrl;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtDensityUrl;
        private System.Windows.Forms.Button bntReprint;
        private System.Windows.Forms.LinkLabel lnkLblOpenErrorLog;
        private System.Windows.Forms.Label lblFlowBinNumber;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblNettMass;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.CheckBox chkScaleLtr;
        private System.Windows.Forms.Label lblLoadedLtr;
        private System.Windows.Forms.TextBox txtLiters;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.CheckBox chkMassRound;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bntScaleSettings;
        private System.Windows.Forms.Label error_Label;
        private System.Windows.Forms.Label lblScale;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lbltank;
        private System.Windows.Forms.CheckBox chkSpecialTest;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.CheckBox chkEnableNetMassCalc;
        private System.Windows.Forms.ComboBox cmbProduct;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ToolStripStatusLabel tsslLiveOrDev;
        private System.Windows.Forms.CheckBox chkManualCapture;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btnCHKManualTests;
    }
}

