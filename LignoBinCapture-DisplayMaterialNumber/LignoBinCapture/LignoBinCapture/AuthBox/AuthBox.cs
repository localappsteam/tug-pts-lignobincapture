﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LignoBinCapture.AuthBox
{
    public partial class AuthBox : Form
    {
        public string Reasontxt { get; set; }
        public Boolean Reasonsuccess { get; set; } = false;
        public string username { get; set; }
        public string empno { get; set; }
        
        public AuthBox()
        {
            InitializeComponent();
        }

        LignoBinCaptures lignoBinCaptures = new LignoBinCaptures();

        private void BntCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntOk_Click(object sender, EventArgs e)
        {
            var reasonbox = new ReasonBox();

            if (!Int32.TryParse(txtEmpN.Text, out int j))
                MessageBox.Show("Login Failed\nMake sure details Entered are correct ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                lignoBinCaptures.Username = txtUsn.Text;
                lignoBinCaptures.Password = txtPsw.Text;
                lignoBinCaptures.EmpNo = txtEmpN.Text;

                if (lignoBinCaptures.AuthCheck() == 1)
                {
                   Hide();
                    reasonbox.ShowDialog();
                    username = txtUsn.Text;
                    empno = txtEmpN.Text;
                    Reasonsuccess = reasonbox.Reasonsuccess;
                    Reasontxt = reasonbox.Reasontxt;
                   
                }
                else
                    MessageBox.Show("Login Failed\nMake sure details Entered are correct", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
        }

        private void AuthBox_Load(object sender, EventArgs e)
        {
            txtUsn.Select();
            txtUsn.Focus();
        }

        private void txtEmpN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
             bntOk.Focus();
            }
        }
    }
}
