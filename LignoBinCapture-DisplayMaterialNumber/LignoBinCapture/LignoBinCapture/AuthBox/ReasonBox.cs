﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LignoBinCapture.AuthBox
{
    public partial class ReasonBox : Form
    {
        public ReasonBox()
        {
            InitializeComponent();
        }


        public Boolean Reasonsuccess { get; set; } = false;
        public string Reasontxt { get; set; }
       private void btnOk_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtReason.Text))          
            {
                Reasonsuccess = true;
                Reasontxt = txtReason.Text;

                Close();
            }
            else
            MessageBox.Show("Suitable Reason was not Entered", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            
            Close();
        }

        private void ReasonBox_Load(object sender, EventArgs e)
        {
            txtReason.Focus();
        }
    }
}
