﻿using System;
using System.IO;

namespace Logit
{
   static class logit
    {
        public static void LogItt(string fullpath, string dirpath, string msg)
        {
            while (true)
            {
                FileInfo fileInfo = new FileInfo(fullpath);
                if (fileInfo.Exists && fileInfo.Length > 9999999L)
                    fileInfo.MoveTo(fullpath.Replace(".txt", "." + DateTime.Now.ToString("yyMMdd")));
                if (!Directory.Exists(dirpath))
                    Directory.CreateDirectory(dirpath);
                else
                    break;
            }
            using (StreamWriter streamWriter = new StreamWriter(fullpath, true))
            {
                streamWriter.WriteLine(msg);
                streamWriter.Close();
            }
        }
    }
}





