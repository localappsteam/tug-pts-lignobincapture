﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LignoBinCapture.RePrint_Form
{
    public partial class frmReprintLabels : Form
    {
        public frmReprintLabels()
        {
            InitializeComponent();
        }
        public string FlowBinNumber { get; set; }
        public Boolean ReprintCheck { get; set; } = false;
        LignoBinCaptures lignoBinCaptures = new LignoBinCaptures();
       
        private void frmReprintLabels_Load(object sender, EventArgs e)
        {
            PopulateReprintDataGrid();
        }

      

        private void PopulateReprintDataGrid()
        {
            dataGridView1.DataSource = lignoBinCaptures.GetInformtionForReprint(txtFlowBinNo.Text, DateTime.Parse(dateTimePicker1.Text).Date);
            dataGridView1.Columns[1].ReadOnly = true;
            dataGridView1.Columns[2].ReadOnly = true;
            dataGridView1.Columns[3].ReadOnly = true;

            dataGridView1.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        }

        private void PrintExtractedFlowBinNumberForReprint()
        {
            Boolean check = false;
           
            var checkedRows = from DataGridViewRow r in dataGridView1.Rows
                              where Convert.ToBoolean(r.Cells[0].Value) == true
                              select r;
            foreach (var row in checkedRows)
            {
                try
                {
                    using (var ligbincap = new LignoBinCap())
                    {
                        
                        ligbincap.flowbinnumber = row.Cells["Flow Bin No"].Value.ToString();
                        lignoBinCaptures.UpdateReprint(ligbincap.flowbinnumber);
                        Logit.logit.LogItt(Properties.Settings.Default.ErrorLogPath, Properties.Settings.Default.ErrorLogloc, DateTime.Now + " --> Warning :"+"Bin label reprinted  " + ligbincap.flowbinnumber);
                        ligbincap.PrintSAPLabel();
                        check = true;
                    }
                }
                catch (Exception ex)
                {
                    check = false;
                }
            }
        }

        private void btnGetFlowBins_Click(object sender, EventArgs e)
        {
            PopulateReprintDataGrid();
        }

        private void btnPrintFlowbins_Click(object sender, EventArgs e)
        {
            PrintExtractedFlowBinNumberForReprint();
            if (MessageBox.Show("Labels have Been sent to Printer\nLeave Reprint Yes\\NO", "Add",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Close();
                ReprintCheck = true;
            }

            ReprintCheck = true;

        }

        private void txtFlowBinNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                PopulateReprintDataGrid();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            
            Close();
        }

        private void dateTimePicker1_MouseDown(object sender, MouseEventArgs e)
        {
            txtFlowBinNo.Text = "";
        }
    }
}

