<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" version="1.0" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<xsl:text/>
		<xsl:apply-templates select="/ROWSET"/>
		<xsl:text/>
	</xsl:template>
	<xsl:template match="/ROWSET">
		<xsl:text>~EXECUTE;</xsl:text>
                <xsl:value-of select="TUGRFSC" />
                <xsl:value-of select="TUGRFSCNL" />
				<xsl:value-of select="TUGLIGNO" />
				<xsl:value-of select="NGXRFSC" />
				<xsl:value-of select="NGXRFSCNL" />
				<xsl:value-of select="STARFSC" />
				<xsl:value-of select="STARFSCNL" />
				<xsl:value-of select="ENSRFSC" />
				<xsl:value-of select="ENSRFSCNL" />
		<xsl:text>;&#13;</xsl:text>
		<xsl:call-template name="fields" /><xsl:text>~NORMAL</xsl:text>
		<xsl:text>&#13;</xsl:text>
	</xsl:template>
	<xsl:template name="fields">
		<xsl:for-each select="*">
			<xsl:if test="name()!='no_labels' and name()!='rejects' and name()!='TUGRFSC' and name()!='TUGRFSCNL' and name()!='TUGLIGNO' and name()!='NGXRFSC' and name()!='NGXRFSCNL' and name()!='STARFSC' and name()!='STARFSCNL' and name()!='ENSRFSC' and name()!='ENSRFSCNL'">
				<xsl:text/>~<xsl:value-of select="name()"/>;"<xsl:value-of select="text()"/>"&#13;<xsl:text/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>

